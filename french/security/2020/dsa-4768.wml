#use wml::debian::translation-check translation="48834d10a4104ac36c3f9d5f545e09374d165f06" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découvertes dans le navigateur
web Firefox de Mozilla, qui pourraient éventuellement avoir pour
conséquence l'exécution de code arbitraire, un script intersite ou
une usurpation de l’origine du téléchargement.</p>

<p>Debian suit les éditions longue durée (ESR, « Extended Support
Release ») de Firefox. Le suivi des séries 68.x est terminé, aussi,
à partir de cette mise à jour, Debian suit les versions 78.x.</p>

<p>Entre les versions 68.x et 78.x, Firefox a vu nombre de ses fonctions
mises à jour. Pour davantage d'informations, veuillez consulter la page
<a href="https://www.mozilla.org/en-US/firefox/78.0esr/releasenotes/">\
https://www.mozilla.org/en-US/firefox/78.0esr/releasenotes/</a>.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 78.3.0esr-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firefox-esr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firefox-esr,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4768.data"
# $Id: $
