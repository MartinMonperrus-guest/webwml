#use wml::debian::translation-check translation="7026d582b1c7da56c7ce3c7ba63f7c8f62f88e59" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Un traitement incorrect de la mémoire dans l'implémentation de
l'émulateur réseau SLiRP pourrait avoir pour conséquence un déni de service
ou éventuellement l'exécution de code arbitraire.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 1:3.1+dfsg-8+deb10u7. En complément, cette mise à jour corrige une
régression causée par le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-13754">\
CVE-2020-13754</a>, qui pourrait conduire à des échecs d'amorçage dans
certaines configurations de Xen.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qemu, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4733.data"
# $Id: $
