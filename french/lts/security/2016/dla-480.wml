#use wml::debian::translation-check translation="c8457a59a89d51637f9c29eabc2b64c8a52130b6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour de sécurité corrige de sérieux problèmes de sécurité
dans NSS y compris des attaques d'exécution de code arbitraire et de déni 
de service distant.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.14.5-1+deb7u6. Nous vous recommandons de mettre à niveau vos
paquets nss paquets dès que possible.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7181">CVE-2015-7181</a>

<p>La fonction sec_asn1d_parse_leaf ne restreint pas correctement l'accès à
une structure de données non précisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7182">CVE-2015-7182</a>

<p>Un dépassement de tas dans le décodeur ASN.1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1938">CVE-2016-1938</a>

<p>La fonction s_mp_div dans lib/freebl/mpi/mpi.c ne divise pas
correctement les nombres, ce qui pourrait faciliter pour des attaquants
distants la casse des mécanismes de protection cryptographique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1950">CVE-2016-1950</a>

<p>Un dépassement de tas permet à des attaquants distants d'exécuter du
code arbitraire au moyen de données ASN.1 contrefaites dans un certificat
X.509.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1978">CVE-2016-1978</a>

<p>Une vulnérabilité d'utilisation de mémoire après libération dans la
fonction ssl3_HandleECDHServerKeyExchange permet à des attaquants distants
de provoquer un déni de service ou d'avoir éventuellement un autre impact
non précisé en réalisant une initialisation de connexion SSL (1) DHE ou
(2) ECDHE à un moment de forte consommation de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1979">CVE-2016-1979</a>

<p>Une vulnérabilité d'utilisation de mémoire après libération dans la
fonction PK11_ImportDERPrivateKeyInfoAndReturnKey permet à des attaquants
distants de provoquer un déni de service ou d'avoir éventuellement un autre
impact non précisé au moyen de données de clé contrefaites avec un encodage
DER.</p></li>

</ul>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-480.data"
# $Id: $
