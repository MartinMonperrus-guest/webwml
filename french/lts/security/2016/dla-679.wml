#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans qemu-kvm :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8576">CVE-2016-8576</a>

<p>qemu-kvm construit avec la prise en charge de l'émulation du contrôleur
USB xHCI est vulnérable à un problème de boucle infinie. Il pourrait se
produire lors du traitement du « ring » de commande USB dans
<q>xhci_ring_fetch</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8577">CVE-2016-8577</a>

<p>qemu-kvm construit avec la prise en charge du dorsal virtio-9p est
vulnérable à un problème de fuite de mémoire. Il pourrait se produire lors
de la réalisation d'une opération de lecture d'entrées et sorties dans la
routine v9fs_read().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8578">CVE-2016-8578</a>

<p>qemu-kvm construit avec la prise en charge du dorsal virtio-9p est
vulnérable à un problème de déréférencement de pointeur NULL. Il pourrait
se produire lors de la réalisation d'une opération de désérialisation d'un
vecteur d'entrées et sorties dans la routine v9fs_iov_vunmarshal().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8669">CVE-2016-8669</a>

<p>qemu-kvm construit avec la prise en charge de l'émulation de 16550A UART
est vulnérable à un problème de division par zéro. Il pourrait se produire
lors de la mise à jour de paramètres de périphériques série dans
<q>serial_update_parameters</q>.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.1.2+dfsg-6+deb7u17.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu-kvm.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-679.data"
# $Id: $
