#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Linux 4.9 a été empaqueté pour Debian 8 sous linux-4.9. Cela fournit un
chemin de gestion de mise à niveau pour des systèmes qui actuellement utilise
les paquets de noyau de la suite « Jessie-backports ».</p>

<p>Cependant, « apt full-upgrade » n’installera *pas* les paquets mis à jour du
noyau. Vous devez d’abord installer explicitement un des paquets suivants,
approprié à votre système :</p>

<ul>
<li>linux-image-4.9-686 ;</li>
<li>linux-image-4.9-686-pae ;</li>
<li>linux-image-4.9-amd64 ;</li>
<li>linux-image-4.9-armmp ;</li>
<li>linux-image-4.9-armmp-lpae ;</li>
<li>linux-image-4.9-marvell.</li>
</ul>

<p>Par exemple, si la commande « uname -r » retourne présentement
« 4.9.0-0.bpo.6-amd64 », vous devriez installer linux-image-4.9-amd64.</p>

<p>Il n’est nullement nécessaire de mettre à niveau les systèmes utilisant
Linux 3.16, car cette version de noyau continuera d’être pris en charge
pendant la période LTS.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p> Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été résolus dans la
version 80+deb9u5~deb8u de linux-latest-4.9.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1424.data"
# $Id: $
