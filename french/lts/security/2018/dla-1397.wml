#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvés dans PHP, un langage de script
polyvalent au code source ouvert largement utilisé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7584">CVE-2018-7584</a>

<p>Un dépassement de tampon de pile lors de l’analyse de réponses HTTP
aboutissait en une copie d’une large chaîne et une possible corruption de
mémoire ou un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10545">CVE-2018-10545</a>

<p>Les processus fils FPM déchargeables permettaient le contournement des
contrôles d'accès d'opcache aboutissant à une divulgation potentielle
d'informations où un utilisateur peut obtenir des informations sur d’autres
applications PHP exécutées par un autre utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10546">CVE-2018-10546</a>

<p>Une séquence non valable d’octets peut déclencher une boucle infinie dans le
filtre de flux convert.iconv.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10547">CVE-2018-10547</a>

<p>Une correction précédente pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-5712">CVE-2018-5712</a>
pourrait ne pas être complète, conduisant à une vulnérabilité supplémentaire
sous la forme d’un XSS réfléchi dans les pages d’erreur 403 et 404 de PHAR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10548">CVE-2018-10548</a>

<p>Un serveur LDAP distant malveillant peut envoyer une réponse contrefaite qui
peut causer un déni de service (déréférencement de pointeur NULL aboutissant à
un plantage d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10549">CVE-2018-10549</a>

<p>Un fichier JPEG contrefait peut causer une lecture hors limites et un
dépassement de tampon basé sur le tas.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 5.6.36+dfsg-0+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1397.data"
# $Id: $
