#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Divers problèmes de sécurité ont été découverts dans Graphicsmagick, une
collection d’outils de traitement d’images.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18219">CVE-2017-18219</a>

<p>Une vulnérabilité d’échec d’allocation a été découverte dans la fonction
ReadOnePNGImage dans coders/png.c. Elle permet à des attaquants de provoquer un
déni de service à l'aide d'un fichier contrefait qui déclenche un essai
d’allocation de grands tableaux png_pixels.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18220">CVE-2017-18220</a>

<p>Les fonctions ReadOneJNGImage et ReadJNGImage dans coders/png.c permettent à
des attaquants distants de provoquer un déni de service ou éventuellement d’avoir
un impact non précisé à l'aide d'un fichier contrefait, un problème relatif à
<a href="https://security-tracker.debian.org/tracker/CVE-2017-11403">CVE-2017-11403</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18229">CVE-2017-18229</a>

<p>Une vulnérabilité d’allocation a été découverte dans la fonction
ReadTIFFImage dans coders/tiff.c. Elle permet à des attaquants de provoquer un
déni de service à l'aide d'un fichier contrefait, parce que la taille d’un
fichier n’est pas correctement utilisée pour restreindre les allocations de
scanline, strip et tile.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18230">CVE-2017-18230</a>

<p>Une vulnérabilité de déréférencement de pointeur NULL a été découverte dans
la fonction ReadCINEONImage dans coders/cineon.c. Elle permet à des attaquants
de provoquer un déni de service à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18231">CVE-2017-18231</a>

<p>Une vulnérabilité de déréférencement de pointeur NULL a été découverte dans
la fonction ReadEnhMetaFile dans coders/emf.c. Elle permet à des attaquants de
provoquer un déni de service à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9018">CVE-2018-9018</a>

<p>Il existe une erreur de division par zéro dans la fonction ReadMNGImage de
coders/png.c. Des attaquants distants pourraient exploiter cette vulnérabilité
pour provoquer un plantage et un déni de service à l'aide d'un fichier mng
contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.3.16-1.1+deb7u19.</p>


<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1322.data"
# $Id: $
