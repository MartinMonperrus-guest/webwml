#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Ming.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11704">CVE-2017-11704</a>

<p>Lecture hors limites de tampon basé sur le tas dans la fonction
decompileIF dans util/decompile.c dans Ming <= 0.4.8. Cela permet à des
attaquants de provoquer un déni de service à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11728">CVE-2017-11728</a>

<p>Lecture hors limites de tampon basé sur le tas dans la fonction OpCode
(appelée à partir de decompileSETMEMBER) dans util/decompile.c dans
Ming <= 0.4.8. Cela permet à des attaquants de provoquer un déni de service à l'aide
d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11729">CVE-2017-11729</a>

<p>Lecture hors limites de tampon basé sur le tas dans la fonction OpCode
(appelée à partir de decompileINCR_DECR ligne 1440) dans util/decompile.c dans
Ming <= 0.4.8. Cela permet à des attaquants de provoquer un déni de service à
l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11730">CVE-2017-11730</a>

<p>Lecture hors limites de tampon basé sur le tas dans la fonction OpCode
(appelée à partir de decompileINCR_DECR ligne 1474) dans util/decompile.c dans
Ming <= 0.4.8. Cela permet à des attaquants de provoquer un déni de service à
l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11731">CVE-2017-11731</a>

<p>Lecture non valable dans la fonction OpCode (appelée à partir de
isLogicalOp et decompileIF) dans util/decompile.c dans Ming <= 0.4.8. Cela
permet à des attaquants de provoquer un déni de service à l'aide d'un fichier
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11734">CVE-2017-11734</a>

<p>Lecture hors limites de tampon basé sur le tas dans la fonction
decompileCALLFUNCTION dans util/decompile.c dans Ming <= 0.4.8. Cela permet à
des attaquants de provoquer un déni de service à l'aide d'un fichier contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:0.4.4-1.1+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ming.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1133.data"
# $Id: $
