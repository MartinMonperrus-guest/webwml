#use wml::debian::translation-check translation="d6728bdd7432b78389b8783b62fd6a96413d384b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans sqlite3, une bibliothèque C
qui met en œuvre un moteur de base de données SQL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8740">CVE-2018-8740</a>

<p>Les bases de données dont le schéma est corrompu utilisant une construction
CREATE TABLE AS pourraient provoquer un déréférencement de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20346">CVE-2018-20346</a>

<p>Lorsque l’extension FTS3 est active, sqlite3 est confrontée à un dépassement
d'entier (et le dépassement de tampon consécutif) pour les requêtes FTS3 qui
surviennent après des modifications faites sur des <q>shadow tables</q>,
permettant à des attaquants distants d’exécuter du code arbitraire en exploitant
la possibilité d’exécuter des constructions SQL arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20506">CVE-2018-20506</a>

<p>Lorsque l’extension FTS3 est active, sqlite3 est confrontée à un dépassement
d'entier (et le dépassement de tampon consécutif) pour les requêtes FTS3 dans
une opération <q>merge</q> qui surviennent après des modifications faites sur
des <q>shadow tables</q>, permettant à des attaquants distants d’exécuter du
code arbitraire en exploitant la possibilité d’exécuter des constructions SQL
arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5827">CVE-2019-5827</a>

<p>Un dépassement d'entier permettrait à un attaquant distant d’éventuellement
exploiter une corruption de tas à l'aide d'une page HTML contrefaite, et
impactait principalement chromium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9936">CVE-2019-9936</a>

<p>L’exécution de requêtes de préfixe fts5 dans une transaction pourrait
déclencher une lecture hors limites de tampon de tas. Cela pourrait conduire à
une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9937">CVE-2019-9937</a>

<p>L’intercalation de lectures et écritures dans une seule transaction avec une
table virtuelle fts5 pourrait conduire à un déréférencement de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16168">CVE-2019-16168</a>

<p>Un navigateur ou une autre application peut être amené à planter à cause
d’une validation inadéquate de paramètre qui pourrait conduire à erreur de
division par zéro.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20218">CVE-2019-20218</a>

<p><q>Unwinding</q> la pile WITH s’exécute même après une erreur d’analyse,
aboutissant à un possible plantage d'application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13630">CVE-2020-13630</a>

<p>Le code relatif à la fonctionnalité snippet expose à un défaut d’utilisation
de mémoire après libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13632">CVE-2020-13632</a>

<p>Une requête matchinfo() contrefaite peut conduire à un déréférencement de
pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13871">CVE-2020-13871</a>

<p>La réécriture de l’arbre d’analyse pour les fonctions de fenêtrage est trop
tardive, conduisant à un défaut d’utilisation de mémoire après libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11655">CVE-2020-11655</a>

<p>Une initialisation incorrecte d’objets AggInfo permet à des attaquants de
provoquer un déni de service (erreur de segmentation) à l'aide d'une requête
de fonction de fenêtrage mal formée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13434">CVE-2020-13434</a>

<p>Le code dans sqlite3_str_vappendf dans printf.c contient un défaut de
dépassement d'entier.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.16.2-5+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sqlite3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sqlite3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sqlite3">https://security-tracker.debian.org/tracker/sqlite3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2340.data"
# $Id: $
