#use wml::debian::translation-check translation="0570e831cf57da095a7eb2edc2a7a2147439fab7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>asyncpg avant sa version 0.21.0 permet à un serveur PostgreSQL malveillant de
déclencher un plantage ou d’exécuter du code arbitraire (dans un client de base
de données) à l'aide d'une réponse de serveur contrefaite, à cause d’un accès à
un pointeur non initialisé dans le décodeur de données de tableau.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.8.4-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets asyncpg.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de asyncpg, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/asyncpg">https://security-tracker.debian.org/tracker/asyncpg</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2363.data"
# $Id: $
