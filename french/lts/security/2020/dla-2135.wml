#use wml::debian::translation-check translation="9a536f0d8196daaf76f08c61323d112478afe288" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Les CVE suivant ont été signalés pour le paquet source jackson-databind.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9546">CVE-2020-9546</a>

<p>jackson-databind 2.x avant 2.9.10.4 de FasterXML gère incorrectement
l’interaction entre des gadgets de sérialisation et la saisie, relatifs à
org.apache.hadoop.shaded.com.zaxxer.hikari.HikariConfig (c'est-à-dire,
shaded hikari-config).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9547">CVE-2020-9547</a>

<p>jackson-databind 2.x avant 2.9.10.4 de FasterXML gère incorrectement
l’interaction entre des gadgets de sérialisation et la saisie, relatifs à
com.ibatis.sqlmap.engine.transaction.jta.JtaTransactionConfig (c'est-à-dire,
ibatis-sqlmap).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9548">CVE-2020-9548</a>

<p>jackson-databind 2.x avant 2.9.10.4 de FasterXML gère incorrectement
l’interaction entre des gadgets de sérialisation et la saisie, relatifs à
br.com.anteros.dbcp.AnterosDBCPConfig (c'est-à-dire, anteros-core).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.4.2-2+deb8u12.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jackson-databind.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2135.data"
# $Id: $
