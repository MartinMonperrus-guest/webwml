#use wml::debian::template title="Debian comme poste de travail"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="fa8e80162815f92b0d792ca4837df20cdc61c896" maintainer="Thomas Huriaux"
# Original translation by Yann Forget

<h2>Le système d'exploitation universel pour votre poste de travail</h2>
<p>
  Le sous-projet Debian Desktop est géré par un groupe de volontaires qui veulent créer le
  meilleur système d'exploitation possible pour un poste de travail pour une
  utilisation personnelle ou professionnelle. Notre devise est <q>Des
  logiciels qui fonctionnent, tout simplement</q>. En bref, notre objectif est
  d'apporter Debian, GNU et Linux à tout le monde.
  <img style="float: right;" src="debian-desktop.png" alt="Debian Desktop" width="371" height="323">
</p>
<h3>Nos principes</h3>
<ul>
  <li>
    Reconnaissant que de nombreux
   <a href="https://wiki.debian.org/DesktopEnvironment">environnements de bureau</a>
    existent, nous prendrons en charge leurs utilisations et nous assurerons
    qu’ils fonctionnent bien dans Debian.
  </li>
  <li>
    Nous reconnaissons qu'il n'y a que deux groupes importants
    d'utilisateurs&nbsp;:
    les débutants et les experts. Nous ferons tout ce que nous pouvons pour
    rendre les choses simples aux débutants, tout en permettant aux
    experts de modifier les choses comme ils le souhaitent.
  </li>
  <li>
     Nous nous assurerons que les logiciels sont configurés pour
     l'utilisation la plus courante d'un poste de travail. Par exemple,
     le compte d’utilisateur ordinaire ajouté par défaut lors de l’installation
     doit permettre de regarder des vidéos, d'écouter de la musique, d'imprimer,
     et de gérer le système avec sudo.
  </li>
  <li>
    <p>
    Nous essaierons d'assurer que les questions posées à l'utilisateur (qui
    doivent être réduites au minimum) aient un sens, même avec peu de
    compétences en informatique. Aujourd'hui, quelques paquets de Debian
    exposent l'utilisateur à des détails techniques difficiles. Les questions
    techniques de debconf posées à l’utilisateur par l’installateur de Debian
    devraient être évitées. Pour le débutant, elles ne font que l’embrouiller et
    l'effrayer. Pour l'expert, elles l'ennuient et ne sont pas nécessaires. Un
    novice peut même ne pas deviner leurs buts. Un expert peut configurer son
    environnement de bureau à sa façon après que l’installation soit terminée.
    La priorité de ces sortes de question de Debconf doit être au moins diminuée.
    </p></li>

 <li>
    Et nous aurons du plaisir à faire tout cela&nbsp;!
  </li>
</ul>
<h3>Comment pouvez-vous aider&nbsp;?</h3>
<p>
  Le plus important d'un sous-projet Debian, ce ne sont pas les listes de
  discussion, le site web, ou l'espace d'archive pour les paquets. Le plus
  important, ce sont les <em>personnes motivées</em> qui font que les choses se
  produisent. Vous n'avez pas besoin d'être un développeur Debian officiel pour
  commencer à faire des paquets et des rustines. Le c&oelig;ur de l'équipe
  Debian Desktop s'assure que votre travail sera intégré. Ainsi, voici
  plusieurs choses que vous pouvez faire&nbsp;:
</p>
<ul>
  <li>
    tester notre tâche «&nbsp;environnement de bureau par défaut&nbsp;»
    (ou la tâche kde-desktop), en installant l'une des
    <a href="$(DEVEL)/debian-installer/">images de test pour la prochaine
    publication</a> et envoyer vos commentaires à la
    <a href="https://lists.debian.org/debian-desktop/">liste
    de diffusion debian-desktop</a>&nbsp;;
  </li>
  <li>
    travailler sur l'<a href="$(DEVEL)/debian-installer/">installateur
    Debian</a>. L'interface GTK+ a besoin de main d'&oelig;uvre&nbsp;;
  <li>
    aider les équipes
    <a href="https://wiki.debian.org/Teams/DebianGnome">Debian GNOME</a>,
    <a href="https://qt-kde-team.pages.debian.net/">Debian Qt et KDE</a> ou
    <a href="https://salsa.debian.org/xfce-team/">Debian Xfce</a>.

    Vous pouvez aider à l'empaquetage, au tri des bogues, à la
    documentation, aux tests, etc.&nbsp;;
  </li>
  <li>
    apprendre aux utilisateurs à installer et à utiliser les tâches pour
    le bureau que nous avons actuellement (desktop, gnome-desktop et
    kde-desktop)&nbsp;;
  </li>
  <li>
    travailler à réduire la priorité, ou à retirer des paquets les écrans
    <a href="https://packages.debian.org/debconf">debconf</a> non
    nécessaires, et rendre ceux qui sont nécessaires faciles à
    comprendre&nbsp;;
  </li>
  <li>
    participer à
    l'<a href="https://wiki.debian.org/DebianDesktop/Artwork">initiative
    artistique pour le bureau Debian</a>.
  </li>
</ul>
<h3>Wiki</h3>
<p>
  Nous avons quelques articles sur notre wiki, notre page principale
  étant <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a>.
  Certains articles ne sont cependant plus à jour.
</p>
<h3>Listes de diffusion</h3>
<p>
  Les discussions de ce sous-projet ont lieu sur la liste de diffusion
  <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.
</p>

<h3>Canal IRC</h3>
<p>
  Nous encourageons toutes les personnes (développeur Debian ou non)
  intéressées par Debian Desktop à rejoindre #debian-desktop sur
  <a href="http://oftc.net/">IRC, réseau OFTC</a> (irc.debian.org).
</p>

<h3>Qui participe&nbsp;?</h3>
<p>
  Tous ceux qui veulent participer sont les bienvenus. En particulier,
  tous ceux qui participent aux sous-projets pkg-gnome, pkg-kde et pkg-xfce
  sont indirectement impliqués. Les abonnés de la liste de diffusion
  debian-desktop sont des contributeurs actifs. Les groupes de
  l'installateur Debian et de tasksel sont également importants pour la
  réalisation de nos objectifs.
</p>

<p>
  Cette page web est maintenue par
  <a href="https://people.debian.org/~stratus/">Gustavo Franco</a>.
  Les responsables précédents étaient
  <a href="https://people.debian.org/~madkiss/">Martin Loschwitz</a> et
  <a href="https://people.debian.org/~walters/">Colin Walters</a>.
</p>
