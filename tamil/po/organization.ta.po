# translation of organization.po to தமிழ்
# ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2007-11-05 15:36+0530\n"
"Last-Translator: ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>\n"
"Language-Team: தமிழ் <ubuntu-l10n-tam@lists.ubuntu.com>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Tamil\n"
"X-Poedit-Country: INDIA\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Generator: KBabel 1.11.4\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr ""

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr ""

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "தற்போதைய"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "உறுப்பினர்"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:41
#, fuzzy
msgid "Stable Release Manager"
msgstr "வெளியீட்டு மேலாளர்"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr ""

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
#, fuzzy
msgid "chair"
msgstr "தலைவர்"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "உதவியாளர்"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "செயலர்"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "அலுவலர்கள்"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "வழங்கல்"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:239
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:242
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:246
#, fuzzy
msgid "Publicity team"
msgstr "விளம்பரம்"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:319
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:347
msgid "Support and Infrastructure"
msgstr "ஆதரவும் கட்டமைப்பும்"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "அணித்தலைவர்"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "தொழில்நுட்பக் குழு"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "செயலர்"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "உருவாக்கத் திட்டங்கள்"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "கோபநெ பெட்டகம்"

#: ../../english/intro/organization.data:114
#, fuzzy
msgid "FTP Masters"
msgstr "கோபநெ முதல்வர்"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "கோபநெ துணைவோர்"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "வெளியீடு நிர்வாகம்"

#: ../../english/intro/organization.data:138
#, fuzzy
msgid "Release Team"
msgstr "வெளியீட்டுக் குறிப்புகள்"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "தர உத்தரவாதம்"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "நிறுவலுக்கான குழுமம்"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "வெளியீட்டுக் குறிப்புகள்"

#: ../../english/intro/organization.data:152
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "குறு வட்டு பிம்பங்கள்"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "உருவாக்கம்"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "சோதனை"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:167
#, fuzzy
msgid "Autobuilding infrastructure"
msgstr "ஆதரவும் கட்டமைப்பும்"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:176
#, fuzzy
msgid "Buildd administration"
msgstr "சமை நிர்வாகம்"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "ஆவணமாக்கம்"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "பணித்-தேவைப் படுபவை மற்றும் வாய்ப்புள்ள பொதிகளின் பட்டியல்"

#: ../../english/intro/organization.data:200
msgid "Ports"
msgstr "துறைகள்"

#: ../../english/intro/organization.data:230
msgid "Special Configurations"
msgstr "சிறப்பு வடிவமைப்புகள்"

#: ../../english/intro/organization.data:232
msgid "Laptops"
msgstr "மடிக்கணினிகள்"

#: ../../english/intro/organization.data:233
msgid "Firewalls"
msgstr "அரண்கள்"

#: ../../english/intro/organization.data:234
msgid "Embedded systems"
msgstr "உட்பொதிந்த சாதனங்கள்"

#: ../../english/intro/organization.data:249
msgid "Press Contact"
msgstr "பத்திரிக்கை தொடர்பு"

#: ../../english/intro/organization.data:251
msgid "Web Pages"
msgstr "இணையப் பக்கங்கள்"

#: ../../english/intro/organization.data:263
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:268
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:273
msgid "Debian Women Project"
msgstr "பெண்களுக்கான டெபியன் திட்டம்"

#: ../../english/intro/organization.data:281
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:290
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Events"
msgstr "சம்பவங்கள்"

#: ../../english/intro/organization.data:299
#, fuzzy
msgid "DebConf Committee"
msgstr "தொழில்நுட்பக் குழு"

#: ../../english/intro/organization.data:306
msgid "Partner Program"
msgstr "பங்குதாரர் திட்டம்"

#: ../../english/intro/organization.data:310
msgid "Hardware Donations Coordination"
msgstr "வன்பொருள் தான ஒருங்கிணைப்பு"

#: ../../english/intro/organization.data:325
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:327
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:329
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:331
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:333
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:334
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:337
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:340
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:343
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:350
msgid "User support"
msgstr "பயனர் ஆதரவு"

#: ../../english/intro/organization.data:417
msgid "Bug Tracking System"
msgstr "வழுக் கண்காணிப்பு அமைப்பு"

#: ../../english/intro/organization.data:422
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "மடலாடற் குழு நிர்வாகம் மற்றும் மடலாடற் குழு பெட்டகம்"

#: ../../english/intro/organization.data:431
#, fuzzy
msgid "New Members Front Desk"
msgstr "புதிய பராமமரிப்பாளர்கள் முன்னணி"

#: ../../english/intro/organization.data:437
msgid "Debian Account Managers"
msgstr "டெபியன் கணக்கு மேலாளர்கள்"

#: ../../english/intro/organization.data:441
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:442
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "துருப்பு பராமரிப்பாளர்கள் (பிஜிபி மற்றும் ஜிபிஜி)"

#: ../../english/intro/organization.data:446
msgid "Security Team"
msgstr "பாதுகாப்புக் குழு"

#: ../../english/intro/organization.data:457
msgid "Consultants Page"
msgstr "ஆலோசகர் பக்கம்"

#: ../../english/intro/organization.data:462
msgid "CD Vendors Page"
msgstr "வட்டு வழங்குவோர் பக்கம்"

#: ../../english/intro/organization.data:465
msgid "Policy"
msgstr "கொள்கை"

#: ../../english/intro/organization.data:468
msgid "System Administration"
msgstr "அமைப்பு நிர்வாகம்"

#: ../../english/intro/organization.data:469
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"ஏதெனுமொரு டெபியன் இயந்திரத்தில், கடவுச்சொல் உள்ளிட்ட கோளாறு ஏற்படின் பயன்படுத்தப் பட "
"வேண்டிய முகவரி இது. இல்லையெனில் தாங்கள் நிறுவிட பொதியொன்றுத் தேவைப் படுகிறது.   "

#: ../../english/intro/organization.data:479
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"டெபியன் இயந்திரங்களில் வன்பிரச்சனைகள் இருப்பின், <a href=\"https://db.debian.org/"
"machines.cgi\">டெபியன் இயந்திரங்கள்</a> பக்கத்தினை அணுகவும், பிரதி இயந்திரத்துக்கான "
"நிர்வாகத் தகவலை அது கொண்டிருக்கலாம்."

#: ../../english/intro/organization.data:480
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP உருவாக்குநர் அடைவு நிர்வாகி"

#: ../../english/intro/organization.data:481
msgid "Mirrors"
msgstr "பிம்பங்கள்"

#: ../../english/intro/organization.data:488
msgid "DNS Maintainer"
msgstr "DNS பராமரிப்பாளர்"

#: ../../english/intro/organization.data:489
msgid "Package Tracking System"
msgstr "பொதி கண்காணிப்பு அமைப்பு"

#: ../../english/intro/organization.data:491
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:498
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:502
#, fuzzy
msgid "Salsa administrators"
msgstr "அலியோத் நிர்வாகிகள்"

#~ msgid "Alioth administrators"
#~ msgstr "அலியோத் நிர்வாகிகள்"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "பாதுகாப்புக் குழு"

#~ msgid "Security Audit Project"
#~ msgstr "பாதுகாப்பு தணிக்கைத் திட்டம்"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "இது முறையான டெபியன் திட்டமல்ல. ஆனால் அது ஒருங்கிணைக்கப் படுவதற்கான நோக்கம் "
#~ "அறிவிக்கப்பட்டுள்ளதுதப் பட்டுள்ளது."

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "டெபியன் பல்லூடக வழங்கல்"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "இலாப நோக்கற்ற நிறுவனங்களுக்கான டெபியன்"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "அகிலத்துக்குமான இயங்கு தளம்"

#~ msgid "Accountant"
#~ msgstr "கணக்காளர்"

#~ msgid "Key Signing Coordination"
#~ msgstr "துருப்பு ஒப்புமை ஒருங்கிணைப்பு"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "தனிப்பட்ட சமைப்புக்கான நிர்வாகியின் பெயரை <a href=\"http://www.buildd.net"
#~ "\">http://www.buildd.net</a> முகவரியில் காணலாம். கட்டமைப்பு ஒன்றையும் "
#~ "வழங்கலொன்றையும் தேர்வு செய்து கிடைக்கக் கூடிய சமைப்புகளையும் அவற்றின் நிர்வாகிகளையும் "
#~ "காணலாம்."

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "குறிப்பிட்ட கட்டமைப்புக்கு ஏற்றாற் போல் சமைத்தலுக்கான நிர்வாகிகளை <genericemail "
#~ "arch@buildd.debian.org> முகவரியில் தொடர்புக் கொள்ளலாம். உதாரணம் <genericemail "
#~ "i386@buildd.debian.org>."

#~ msgid "Security Testing Team"
#~ msgstr "பாதுகாப்புச் சோதனைக் குழு"

#~ msgid "Handhelds"
#~ msgstr "கையடக்க"

#~ msgid "APT Team"
#~ msgstr "ஏபிடி குழு"

#~ msgid "Vendors"
#~ msgstr "வழங்குவோர்"

#~ msgid "Release Wizard"
#~ msgstr "வெளியீட்டு பலகை"

#~ msgid "Release Assistants for ``stable''"
#~ msgstr "``நிலையானவைக்கான'' வெளியீட்டு உதவியாளர்கள்"

#~ msgid "Release Assistants"
#~ msgstr "வெளியீட்டு உதவியாளர்கள்"

#~ msgid "Release Managers for ``stable''"
#~ msgstr "\"நிலையானவை\" க்கான வெளியீட்டு மேலாளர்"

#~ msgid "Custom Debian Distributions"
#~ msgstr "தனிப்பட்ட டெபியன் வழங்கல்கள்"

#~ msgid "Publicity"
#~ msgstr "விளம்பரம்"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "நிறுவலுக்கான குழுமம்"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "கல்விக்கான டெபியன்"

#, fuzzy
#~| msgid "Debian for medical practice and research"
#~ msgid "Debian for science and related research"
#~ msgstr "மருத்துவப் பழகலுக்கும் ஆராய்ச்சிக்கும் டெபியன்"

#~ msgid "Debian for people with disabilities"
#~ msgstr "ஊனமுற்றோருக்கான டெபியன்"

#~ msgid "Debian in legal offices"
#~ msgstr "சட்ட அலுவலகங்களில் டெபியன்"

#~ msgid "Debian for education"
#~ msgstr "கல்விக்கான டெபியன்"

#~ msgid "Debian for medical practice and research"
#~ msgstr "மருத்துவப் பழகலுக்கும் ஆராய்ச்சிக்கும் டெபியன்"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "1 முதல் 99 வரைக் குழந்தைகளுக்கான டெபியன்"

#~ msgid "Individual Packages"
#~ msgstr "தனிப்பட்ட பொதிகள்"
