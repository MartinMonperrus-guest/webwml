<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Three vulnerabilities have been discovered in the Go programming language;
"net/url" accepted some invalid hosts in URLs which could result in
authorisation bypass in some applications and the HTTP/2 implementation
was susceptible to denial of service.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1.11.6-1+deb10u1.</p>

<p>We recommend that you upgrade your golang-1.11 packages.</p>

<p>For the detailed security status of golang-1.11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-1.11">\
https://security-tracker.debian.org/tracker/golang-1.11</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4503.data"
# $Id: $
