<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The simplesamlphp package in wheezy is vulnerable to multiple attacks
on authentication-related code, leading to unauthorized access and
information disclosure.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12867">CVE-2017-12867</a>

    <p>The SimpleSAML_Auth_TimeLimitedToken class allows attackers with
    access to a secret token to extend its validity period by manipulating
    the prepended time offset.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12869">CVE-2017-12869</a>

    <p>The multiauth module allows remote attackers to bypass authentication
    context restrictions and use an authentication source defined in
    config/authsources.php via vectors related to improper validation of
    user input.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12872">CVE-2017-12872</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-12868">CVE-2017-12868</a>

    <p>The (1) Htpasswd authentication source in the authcrypt module and (2)
    SimpleSAML_Session class in SimpleSAMLphp 1.14.11 and earlier allow
    remote iattackers to conduct timing side-channel attacks by leveraging
    use of the standard comparison operator to compare secret material
    against user input.</p>

    <p><a href="https://security-tracker.debian.org/tracker/CVE-2017-12868">CVE-2017-12868</a> 
    was a about an improper fix of <a href="https://security-tracker.debian.org/tracker/CVE-2017-12872">CVE-2017-12872</a> in the
    initial patch released by upstream. We have used the correct patch.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12873">CVE-2017-12873</a>

    <p>SimpleSAMLphp might allow attackers to obtain sensitive information,
    gain unauthorized access, or have unspecified other impacts by
    leveraging incorrect persistent NameID generation when an Identity
    Provider (IdP) is misconfigured.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12874">CVE-2017-12874</a>

    <p>The InfoCard module for SimpleSAMLphp allows attackers to spoof
    XML messages by leveraging an incorrect check of return values in
    signature validation utilities.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.9.2-1+deb7u1.</p>

<p>We recommend that you upgrade your simplesamlphp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1205.data"
# $Id: $
