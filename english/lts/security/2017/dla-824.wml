<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in libevent, an asynchronous
event notification library. They would lead to Denial Of Service via
application crash, or remote code execution.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.0.19-stable-3+deb7u2.</p>

<p>We recommend that you upgrade your libevent packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-824.data"
# $Id: $
