<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A regression was added by the patch introduced in version 0.5.0-2+deb7u2 to
fix <a href="https://security-tracker.debian.org/tracker/CVE-2017-16927">CVE-2017-16927</a>: 
xrdp-sesman started to segfault in libscp.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.5.0-2+deb7u3.</p>

<p>We recommend that you upgrade your xrdp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1203-2.data"
# $Id: $
