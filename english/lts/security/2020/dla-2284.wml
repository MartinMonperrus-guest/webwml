<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A flaw was found in the way it evaluates certain
environment variables. An attacker could use this
flaw to override or bypass environment restrictions
to execute shell commands. Services and
applications that allow remote unauthenticated
attackers to provide one of those environment
variables could allow them to exploit this issue
remotely.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
93u+20120801-3.1+deb9u1.</p>

<p>We recommend that you upgrade your ksh packages.</p>

<p>For the detailed security status of ksh please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ksh">https://security-tracker.debian.org/tracker/ksh</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2284.data"
# $Id: $
