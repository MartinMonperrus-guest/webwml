#use wml::debian::template title="Debian 10 &mdash; Известные ошибки" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="69d9136a148e47ae7e1a071118ba614793e75a22" maintainer="Lev Lamberov"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Известные проблемы</toc-add-entry>
<toc-add-entry name="security">Проблемы безопасности</toc-add-entry>

<p>Команда безопасности Debian выпускает обновления пакетов в стабильном выпуске,
в которых они обнаружили проблемы, относящиеся к безопасности. Информацию о всех
проблемах безопасности, найденных в <q>buster</q>, смотрите на
<a href="$(HOME)/security/">страницах безопасности</a>.</p>

<p>Если вы используете APT, добавьте следующую строку в <tt>/etc/apt/sources.list</tt>,
чтобы получить доступ к последним обновлениям безопасности:</p>

<pre>
  deb http://security.debian.org/ buster/updates main contrib non-free
</pre>

<p>После этого запустите <kbd>apt update</kbd> и затем
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Редакции выпусков</toc-add-entry>

<p>Иногда, в случаях множества критических проблем или обновлений безопасности, выпущенный
дистрибутив обновляется. Обычно эти выпуски обозначаются как
редакции выпусков.</p>

<ul>
  <li>Первая редация, 10.1, была выпущена
      <a href="$(HOME)/News/2019/20190907">7 сентября 2019 года</a>.</li>
  <li>Вторая редация, 10.2, была выпущена
      <a href="$(HOME)/News/2019/20191116">16 ноября 2019 года</a>.</li>
  <li>Третья редация, 10.3, была выпущена
      <a href="$(HOME)/News/2020/20200208">8 февраля 2020 года</a>.</li>
  <li>Четвёртая редакция, 10.4, была выпущена
      <a href="$(HOME)/News/2020/20200509">9 мая 2020 года</a>.</li>
  <li>Пятая редакция, 10.5, была выпущена
      <a href="$(HOME)/News/2020/20200801">1 августа 2020 года</a>.</li>
</ul>

<ifeq <current_release_buster> 10.0 "

<p>Для выпуска Debian 10 пока нет редакций.</p>" "

<p>Подробную информацию об изменениях между версиями 10 и <current_release_buster/> смотрите в <a
href=http://http.us.debian.org/debian/dists/buster/ChangeLog>\
журнале
изменений</a>.</p>"/>


<p>Исправления выпущенного стабильного дистрибутива часто должны пройти
усиленное тестирование, прежде чем они будут помещены в архив. Тем не менее,
эти исправления уже доступны в каталоге
<a href="http://ftp.debian.org/debian/dists/buster-proposed-updates/">\
dists/buster-proposed-updates</a> на всех зеркалах
Debian.</p>

<p>Если для обновления пакетов вы используете APT, то можете
установить предлагаемые обновления, добавив следующую строку в файл
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# предлагаемые дополнения для редакции 10-ого выпуска
  deb http://deb.debian.org/debian buster-proposed-updates main contrib non-free
</pre>

<p>После этого запустите <kbd>apt update</kbd> и затем
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Система установки</toc-add-entry>

<p>
Информацию об известных ошибках и обновлениях в системе установки смотрите
на страницах <a href="debian-installer/">системы установки</a>.
</p>
