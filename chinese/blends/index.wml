#use wml::debian::template title="Debian Pure Blends" BARETITLE="true"
#use wml::debian::translation-check translation="6d2e0afb56e760364713c2cca2c9f6407c9a744f"

<p>
Debian Pure Blends 是为有特定需要的人群提供的一套解决方案。\
它们不仅提供特定软件包的方便集合（元软件包），\
而且为所针对的目的简化了安装和配置过程。\
它们覆盖了不同人群的兴趣，包括儿童、科学家、游戏玩家、\
律师、医疗从业者、视觉障碍人士，等等。\
它们的共同目标是为目标受众简化计算机的安装和管理过程，\
并使目标受众能与编写和打包他们所使用的软件的人员保持联系。
</p>

<p>您可以在 <a href="https://blends.debian.org/blends/">Pure Blends 指南\
</a>中找到关于 Debian Pure Blends 的更多信息。</p>

<ul class="toc">
<li><a href="#released">已发布的 Pure Blends</a></li>
<li><a href="#unreleased">即将发布的 Pure Blends</a></li>
<li><a href="#development">开发</a></li>
</ul>

<div class="card" id="released">
<h2>已发布的 Pure Blends</h2>
<div>
<p>“已发布”对于不同的 blends 可能有不同的含义。大多数情况下\
这意味着该 blend 的元软件包或安装程序已经出现在了 Debian 的\
稳定版本中。Blend 也可能会提供安装[CN:介质:][HKTW:媒介:]，或\
作为另一个衍生发行版的基础。欲了解特定 blend 的信息，请参见\
各个 blend 的单独页面。</p>

<table class="tabular" summary="">
<tbody>
 <tr>
  <th>Blend</th>
  <th>描述</th>
  <th>快速链接</th>
 </tr>
#include "../../english/blends/released.data"
</tbody>
</table>
</div>
</div>

<div class="card" id="unreleased">
  <h2>即将发布的 Pure Blends</h2>
  <div>
   <p>这些 blends 正在开发中，尚未有稳定版本发布，尽管它们可能在<a
href="https://www.debian.org/releases/testing/">测试版</a>或者<a
href="https://www.debian.org/releases/unstable">不稳定版</a>中可用。\
一些即将发布的 Pure Blends 可能仍依赖于非自由组件。</p>
  
<table class="tabular" summary="">
<tbody>
 <tr>
  <th>Blend</th>
  <th>描述</th>
  <th>快速链接</th>
 </tr>
#include "../../english/blends/unreleased.data"
</tbody>
</table>
  </div>
 </div>
 
 <div class="card" id="development">
  <h2>开发</h2>
  <div>
   <p>如果您对参与 Debian Pure Blends 的开发感兴趣，\
您可以在<a href="https://wiki.debian.org/DebianPureBlends">维基页面</a>\
找到与开发相关的信息。
   </p>
  </div>
</div><!-- #main -->

