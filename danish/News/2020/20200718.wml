#use wml::debian::translation-check translation="b51002b9cd5b7dbca0e39ddb2d17bcc495ead21a"
<define-tag pagetitle>Opdateret Debian 9: 9.13 udgivet</define-tag>
<define-tag release_date>2020-07-18</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian-projektet er stolt over at kunne annoncere den trettende (og sidste) 
opdatering af dets gamle stabile distribution, Debian <release> (kodenavn 
<q><codename></q>).  
Denne opdatering indeholder primært rettelser af sikkerhedsproblemer, sammen med 
nogle få rettelser af alvorlige problemer.  Sikkerhedsbulletiner er allerede 
udgivet separat og der vil blive refereret til dem, hvor de er tilgængelige.</p>

<p>Efter denne punktopdatering, vil Debians Security og Release Teams ikke 
længere fremstille opdatering til Debian 9.  Brugere, der ønsker at fortsætte 
med at modtage sikkerhedsunderstøttelse, bør opgradere til Debian 10, eller se 
<url "https://wiki.debian.org/LTS"> for oplysninger om den delmængde af 
arkitekturer og pakker, som er dækket af Long Term Support-projektet.</p>

<p>Bemærk at denne opdatering ikke er en ny udgave af Debian GNU/Linux
<release>, den indeholder blot opdateringer af nogle af de medfølgende pakker.  
Der er ingen grund til at smide gamle <q><codename></q>-medier væk.  Efter en 
installering, kan pakkerne opgradere til de aktuelle versioner ved hjælp af et 
ajourført Debian-filspejl.</p>

<p>Dem der hyppigt opdaterer fra security.debian.org, behøver ikke at opdatere 
ret mange pakker, og de fleste opdateringer fra security.debian.org er indeholdt 
i denne opdatering.</p>

<p>Nye installeringsfilaftryk vil snart være tilgængelige fra de sædvanlige 
steder.</p>

<p>Online-opdatering til denne revision gøres normalt ved at lade 
pakkehåndteringssystemet pege på et af Debians mange HTTP-filspejle. En 
omfattende liste over filspejle er tilgængelig på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Forskellige fejlrettelser</h2>

<p>Denne opdatering til den gamle stabile udgave tilføjer nogle få vigtige rettelser 
til følgende pakker:</p>

<table border=0>
<tr><th>Pakke</th>					<th>Årsag</th></tr>
<correction acmetool 					"Genopbygger mod nyere golang for at samle sikkerhedsrettelser op">
<correction atril 					"dvi: Afhjælper kommandoindsprøjtningsangreb ved at sætte anførselstegn om filnavn [CVE-2017-1000159]; retter overløbskontroller i tiff-backend [CVE-2019-1010006]; tiff: Håndterer fejl fra TIFFReadRGBAImageOriented [CVE-2019-11459]">
<correction bacula 					"Tilføjer overgangspakke bacula-director-common, undgår tab af /etc/bacula/bacula-dir.conf ved purge; gør root til ejer af PID-filer">
<correction base-files 					"Opdaterer /etc/debian_version til denne punktopdatering">
<correction batik 					"Retter forfalskning på tværs af servere gennem xlink:href-attributter [CVE-2019-17566]">
<correction c-icap-modules 				"Understøtter ClamAV 0.102">
<correction ca-certificates 				"Opdaterer Mozilla CA-bundle til 2.40, sortlister Symantec-roots som der ikke er tillid til, samt udløbet <q>AddTrust External Root</q>; fjener kun-mail-certifikater">
<correction chasquid 					"Genopbygger mod nyere golang for at samle sikkerhedsrettelser op">
<correction checkstyle 					"Retter XML External Entity-indsprøjtningsproblem [CVE-2019-9658 CVE-2019-10782]">
<correction clamav 					"Ny opstrømsugave [CVE-2020-3123]; sikkerhedsrettelser [CVE-2020-3327 CVE-2020-3341]">
<correction compactheader 				"Ny opstrømsversion, kompatibel med nyere versioner af Thunderbird">
<correction cram 					"Ignorerer testfejl for at rette opbygningsproblemer">
<correction csync2 					"Får HELLO-kommando til at fejle når SSL er krævet">
<correction cups 					"Retter heapbufferoverløb [CVE-2020-3898] og funktionen `ippReadIO` kunne underlæse et udvidelsesfelt [CVE-2019-8842]">
<correction dbus 					"Ny stabil opstrømsudgave; forhindrer lammelsesangrebsproblem [CVE-2020-12049]; forhindrer anvendelse efter frigivelse hvis to brugernavne deler en uid">
<correction debian-installer 				"Opdaterer til Linux-kerne-ABI 4.9.0-13">
<correction debian-installer-netboot-images 		"Genopbygger mod stretch-proposed-updates">
<correction debian-security-support 			"Opdaterer flere pakkers supportstatus">
<correction erlang 					"Retter anvendelse af svage TLS-ciphers [CVE-2020-12872]">
<correction exiv2 					"Retter lammelsesangrebsproblem [CVE-2018-16336]; retter for restriktiv rettelse af CVE-2018-10958 og CVE-2018-10999">
<correction fex 					"Sikkerhedsopdatering">
<correction file-roller 				"Sikkerhedsrettelse [CVE-2020-11736]">
<correction fwupd 					"Ny opstrømsudgave; anvender et CNAME til at viderestille til den korrekte metadata-CDN; afbryd ikke starten hvis XML-metadatafilen er ugyldig; tilføjer Linux Foundations offentlige GPG-nøgler til firmware og metadata; forøger metadatagrænsen til 10MB">
<correction glib-networking 				"Returnerer en bad identity-fejl hvis identity ikke er opsat [CVE-2020-13645]">
<correction gnutls28 					"Retter hukommelseskorruptionsproblem [CVE-2019-3829]; retter hukommelseslækage; tilføjer understøttelse af nul længde-sessiontickets, retter forbindelsesfejl ved TLS1.2-sessioner til de samme hosting providers">
<correction gosa 					"Strammer kontrol af LDAP-succes/fejl [CVE-2019-11187]; retter kompabilitet med nyere PHP-versioner; tilbagefører flere andre patches; erstatter (un)serialize med json_encode/json_decode for at afhjælpe PHP-objektindsprøjtning [CVE-2019-14466]">
<correction heartbleeder 				"Genopbygger mod nyere golang for at samle sikkerhedsrettelser op">
<correction intel-microcode 				"Nedgraderer nogle microcodes til tidligere udgivne versioner, for at omgå hængende system ved boot på Skylake-U/Y og Skylake Xeon E3">
<correction iptables-persistent 			"Der skal ikke fejles hvis modprobe gør det">
<correction jackson-databind 				"Retter adskillige sikkerhedsproblemer som påvirker BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 og CVE-2019-17267]">
<correction libbusiness-hours-perl 			"Anvender eksplicit årstal på fire cifre, retter opbygnings- og anvendelsesproblemer">
<correction libclamunrar 				"Ny stabil opstrømsudgave; tilføjer en ikke-versioneret metapakke">
<correction libdbi 					"Udkommenterer igen kaldet _error_handler(), retter problemer med consumers">
<correction libembperl-perl 				"Håndterer fejlsider fra Apache &gt;= 2.4.40">
<correction libexif 					"Sikkerhedsrettelser [CVE-2016-6328 CVE-2017-7544 CVE-2018-20030 CVE-2020-12767 CVE-2020-0093]; sikkerhedsrettelser [CVE-2020-13112 CVE-2020-13113 CVE-2020-13114]; retter et bufferlæsningsoverløb [CVE-2020-0182] og et overløb ved heltal uden fortegn [CVE-2020-0198]">
<correction libvncserver 				"Retter heapoverløb [CVE-2019-15690]">
<correction linux 					"Ny stabil opstrømsudgave; opdaterer ABI til 4.9.0-13">
<correction linux-latest 				"Opdatering til kerne-ABI 4.9.0-13">
<correction mariadb-10.1 				"Ny stailb opstrømsudgave; sikkerhedsrettelser [CVE-2020-2752 CVE-2020-2812 CVE-2020-2814]">
<correction megatools 					"Tilføjer understøttelse af mega.nz's nye linkformat">
<correction mod-gnutls 					"Undgår udfasede ciphersuites i testsuite; retter testfejl når kombineret med Apaches rettelse af CVE-2019-10092">
<correction mongo-tools 				"Genopbygger mod nyere golang for at samle sikkerhedsrettelser op">
<correction neon27 					"Opfatter OpenSSL-relaterede testfejl som ikke-fatale">
<correction nfs-utils 					"Retter potentiel filoverskrivningssårbarhed [CVE-2019-3689]; overfør ikke hele ejerskabet af /var/lib/nfs til statd-brugeren">
<correction nginx 					"Retter sårbarhed i forbindelse med fejlsideforespørgselssmugling [CVE-2019-20372]">
<correction node-url-parse 				"Renser stier og værter før de fortolkes [CVE-2018-3774]">
<correction nvidia-graphics-drivers 			"Ny stabil opstrømsudgave; ny opstrømsudgave; sikkerhedsrettelser [CVE-2020-5963 CVE-2020-5967]">
<correction pcl 					"Retter manglende afhængighed af libvtk6-qt-dev">
<correction perl 					"Retter adskillige regulære udtræk med forbindelse til sikkerhedsproblemer [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde 					"Retter sårbarhed i forbindelse med udførelse af skripter på tværs af websteder [CVE-2020-8035]">
<correction php-horde-data 				"Retter sårbarhed i forbindelse med autentificeret fjernudførelse af kode [CVE-2020-8518]">
<correction php-horde-form 				"Retter sårbarhed i forbindelse med autentificeret fjernudførelse af kode [CVE-2020-8866]">
<correction php-horde-gollem 				"Retter sårbarhed i forbindelse med udførelse af skripter på tværs af websteder i breadcrumb-uddata [CVE-2020-8034]">
<correction php-horde-trean 				"Retter sårbarhed i forbindelse med autentificeret fjernudførelse af kode [CVE-2020-8865]">
<correction phpmyadmin 					"Flere sikkerhedsrettelser [CVE-2018-19968 CVE-2018-19970 CVE-2018-7260 CVE-2019-11768 CVE-2019-12616 CVE-2019-6798 CVE-2019-6799 CVE-2020-10802 CVE-2020-10803 CVE-2020-10804 CVE-2020-5504]">
<correction postfix 					"Ny stabil opstrømsudgave">
<correction proftpd-dfsg 				"Retter håndtering af SSH_MSG_IGNORE-pakker">
<correction python-icalendar 				"Retter Python3-afhængigheder">
<correction rails 					"Retter mulig udførelse af skripter på tværs af websteder gennem Javascript-escapehelper [CVE-2020-5267]">
<correction rake 					"Retter kommandoindsprøjtningssårbarhed [CVE-2020-8130]">
<correction roundcube 					"Retter udførelse af skripter på tværs af websteder gennem HTML-meddelelser med ondsindet svg/namespace [CVE-2020-15562]">
<correction ruby-json 					"Retter sårbarhed i forbindelse med usikker objektoprettelse [CVE-2020-10663]">
<correction ruby2.3 					"Retter sårbarhed i forbindelse med usikker objektoprettelse [CVE-2020-10663]">
<correction sendmail 					"Retter fremfinding af queue runner-kontrolproces i <q>split daemon</q>-tilstand, <q>NOQUEUE: connect from (null)</q>, fjernelse af fejl ved anvendelse af BTRFS">
<correction sogo-connector 				"Ny opstrømsversion, kompatibel med nyere versioner af Thunderbird">
<correction ssvnc 					"Retter skrivning udenfor grænserne [CVE-2018-20020], uendelig løkke [CVE-2018-20021], ukorrekt initialisering [CVE-2018-20022], potentielt lammelsesangreb [CVE-2018-20024]">
<correction storebackup 				"Retter mulig rettighedsforøgelsessårbarhed [CVE-2020-7040]">
<correction swt-gtk 					"Retter manglende afhængighed af libwebkitgtk-1.0-0">
<correction tinyproxy 					"Opretter PID-fil før rettighederne til ikke-root-konto smides væk [CVE-2017-11747]">
<correction tzdata 					"Ny stabil opstrømsudgave">
<correction websockify 					"Retter manglende afhængighed af python{3,}-pkg-resources">
<correction wpa 					"Retter omgåelse af PMF-disconnection protection i AP-tilstand [CVE-2019-16275]; retter MAC-tilfældighedsproblem med nogle kort">
<correction xdg-utils 					"Renser vindues navn før det sendes over D-Bus; håndterer på korrekt vis mapper med navne indeholdende mellemrum; opretter om nødvendigt mappen <q>applications</q>">
<correction xml-security-c 				"Retter længdeberegning i concat-metoden">
<correction xtrlock 					"Retter blokering af (nogle) multitouch-enheder når låst [CVE-2016-10894]">
</table>


<h2>Sikkerhedsopdateringer</h2>

<p>Denne revision tilføjer følgende sikkerhedsopdateringer til den gamle stabile 
udgave.  Sikkerhedsteamet har allerede udgivet bulletiner for hver af de nævnte
opdateringer:</p>

<table border=0>
<tr><th>Bulletin-id</th>  <th>Pakke(r)</th></tr>
<dsa 2017 4005 openjfx>
<dsa 2018 4255 ant>
<dsa 2018 4352 chromium-browser>
<dsa 2019 4379 golang-1.7>
<dsa 2019 4380 golang-1.8>
<dsa 2019 4395 chromium>
<dsa 2019 4421 chromium>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4621 openjdk-8>
<dsa 2020 4622 postgresql-9.6>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4628 php7.0>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4642 thunderbird>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4666 openldap>
<dsa 2020 4668 openjdk-8>
<dsa 2020 4670 tiff>
<dsa 2020 4671 vlc>
<dsa 2020 4673 tomcat8>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4683 thunderbird>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4692 netqmail>
<dsa 2020 4693 drupal7>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4698 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4703 mysql-connector-java>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4706 drupal7>
<dsa 2020 4707 mutt>
<dsa 2020 4711 coturn>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4715 imagemagick>
<dsa 2020 4717 php7.0>
<dsa 2020 4718 thunderbird>
</table>


<h2>Fjernede pakker</h2>

<p>Følgende pakker er blevet fjernet på grund af omstændigheder uden for vores 
kontrol:</p>

<table border=0>
<tr><th>Pakke</th>			<th>Årsag</th></tr>
<correction certificatepatrol 		"Ikke kompatibel med nyere versioner af Firefox ESR">
<correction colorediffs-extension 	"Ikke kompatibel med nyere versioner af Thunderbird">
<correction dynalogin 			"Afhænging af simpleid som skal fjernes">
<correction enigmail 			"Ikke kompatibel med nyere versioner af Thunderbird">
<correction firefox-esr 		"[armel] No longer supported (requires nodejs)">
<correction firefox-esr 		"[mips mipsel mips64el] Understøttes ikke længere (kræver nyere rustc)">
<correction getlive 			"Defekt pga. ændringer af Hotmail">
<correction gplaycli 			"Defekt pga. ændringer i Google API">
<correction kerneloops 			"Opstrømstjenste er ikke længere tilgængelig">
<correction libmicrodns 		"Sikkerhedsproblemer">
<correction libperlspeak-perl 		"Sikkerhedsproblemer; vedligeholdes ikke">
<correction mathematica-fonts 		"Afhængig af utilgængeligt downloadsted">
<correction pdns-recursor 		"Sikkerhedsproblemer; ingen understøttelse">
<correction predictprotein 		"Afhængig af profphd som skal fjernes">
<correction profphd 			"Ubrugelig">
<correction quotecolors 		"Ikke kompatibel med nyere versioner af Thunderbird">
<correction selenium-firefoxdriver 	"Ikke kompatibel med nyere versioner af Thunderbird">
<correction simpleid 			"Fungerer ikke med PHP7">
<correction simpleid-ldap 		"Afhængig af simpleid som skal fjernes">
<correction torbirdy 			"Ikke kompatibel med nyere versioner af Thunderbird">
<correction weboob 			"Vedligeholdes ikke; allerede fjernet fra senere udgivelser">
<correction yahoo2mbox 			"Har været defekt i flere år">
</table>


<h2>Debian Installer</h2>

<p>Installeringsprogrammet er opdateret for at medtage rettelser indført i oldstable, 
i denne punktopdatering.</p>


<h2>URL'er</h2>

<p>Den komplette liste over pakker, som er ændret i forbindelse med denne 
revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuelle gamle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Foreslåede opdateringer til den gamle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Oplysninger om den gamle stabile distribution (udgivelsesbemærkninger, fejl, 
osv.):</p>

<div class="center">
  <a href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Sikkerhedsannonceringer og -oplysninger:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt; eller kontakt holdet bag den stabile udgave på 
&lt;debian-release@debian.org&gt;.</p>
