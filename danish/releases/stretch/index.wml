#use wml::debian::template title="Debian &ldquo;stretch&rdquo;-udgivelsesoplysninger"
#use wml::debian::translation-check translation="727dca1e3303ed66b1af9f096da919caee43f43c"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/stretch/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"

<p>Debian <current_release_stretch> 
blev udgivet den <a href="$(HOME)/News/<current_release_newsurl_stretch/>"><current_release_date_stretch></a>.
<ifneq "9.0" "<current_release>"
  "Debian 9.0 blev oprindelig udgivet den <:=spokendate('2017-06-17'):>."
/>
Udgivelsen indeholdt mange større ændringer, som er beskrevet i vores 
<a href="$(HOME)/News/2017/20170617">pressemeddelelse</a> og i 
<a href="releasenotes">Udgivelsesbemærkningerne</a>.</p>

<p><strong>Debian 9 er erstattet af
<a href="../buster/">Debian 10 (<q>buster</q>)</a>.
Sikkerhedsopdateinger er ophørt pr. <:=spokendate('2020-07-06'):>.
</strong></p>

<p><strong>Stretch drager også nytte af Long Term Support (LTS) indtil
udgangen af juni 2022.  LTS er begrænset til i386, amd64, armel, armhf og arm64.
Alle andre arkitekturer er ikke længere understøttet i stretch.
For flere oplysninger, se 
<a href="https://wiki.debian.org/LTS">afsnittet LTS i Debian Wiki</a>.
</strong></p>

<p>For at hente og installere Debian, se siden med 
<a href="debian-installer/">installeringsoplysninger</a> og 
<a href="installmanual">Installeringsvejledningen</a>.  For at opgradere fra 
en ældre Debian-udgive, se vejledningen i <a href="releasenotes">\
Udgivelsesbemærkningerne</a>.</p>

# Activate the following when LTS period starts.
<p>Arkitekturer understøttet under Long Term Support:</p>

<ul>
<:
foreach $arch (@archeslts) {
       print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Computerarkitekturer understøttet i den første udgave af stretch:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Imod vores ønsker, kan der være tilbageværende problemer i udgivelsen, selv 
om den er blevet erklæret <em>stabil</em>.  Vi har lavet en <a href="errata">\
liste over de største kendte problemer</a>, og du kan kan altid 
<a href="reportingbugs">rapportere andre problemer</a> til os.</p>

<p>Sidst men ikke mindst, har vi en liste over <a href="credits">folk der har gjort 
denne udgave mulig</a>.</p>

<if-stable-release release="wheezy">
<p>Endnu ingen tilgængelige oplysninger.</p>
</if-stable-release>

<if-stable-release release="jessie">
<p>Kodenavnet på den næste store Debian-udgivelse efter <a href="../jessie/">\
jessie</a> er <q>stretch</q>.</p>

<p>Denne udgave begyndte som en kopi af jessie, og er pt. i en fase kaldet
<q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">testing</a></q> 
(i test).  Det betyder at ting ikke burde holde op med at virke på så slem en 
måde, som i den ustabile eller eksperimentelle distribution, fordi pakkerne kun 
får lov til at blive optaget i distributionen efter et givet tidsrum er gået, og 
når der ikke er indsendt udgivelseskritiske fejl mod dem.</p>

<p>Bemærk venligst at sikkerhedsopdateringer til <q>testing</q>-distributionen 
endnu <strong>ikke</strong> håndteres af sikkerhedsteamet.  Derfor modtager 
<q>testing</q> <strong>ikke</strong> jævnlige sikkerhedsopdateringer.  
# For flere oplysninger, se 
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">\
# annonceringen</a> fra Testing Security Team.
Du opfordres til at ændre dine sources.list-linjer fra testing til wheezy indtil 
videre, hvis du har brug for sikkerhedsunderstøttelse.  Se også punktet i 
<a href="$(HOME)/security/faq#testing">sikkerhedsteamets OSS</a> vedrørende
<q>testing</q>-distributionen.</p>

<p>Der findes måske et <a href="releasenotes">udkast til 
udgivelsesbemærkninger</a>.  Se også de 
<a href="https://bugs.debian.org/release-notes">foreslåede tilføjelser til
udgivelsesbemærkningerne</a>.</p>

<p>Hvad angår installationsaftryk og -dokumentation om hvordan man installerer 
<q>testing</q>, se <a href="$(HOME)/devel/debian-installer/">siden om 
Debian-Installer</a>.</p>

<p>For at få flere oplysninger om hvordan <q>testing</q>-distributionen fungerer, se
<a href="$(HOME)/devel/testing">oplysningerne til udviklerne om den</a>.</p>

<p>Folk spørger ofte om der findes en indikator for hvor langt vi er nået med
udgaven.  Desværre findes der ikke sådan en, men vi kan henvise til flere 
steder som beskriver ting der skal tages hånd om før udgivelsen kan ske:</p>

<ul>
  <li><a href="https://release.debian.org/">Generisk udgivelses-statusside</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Udgivelseskritiske fejl</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Fejl i grundsystemet</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Fejl i standard- og task-pakker</a></li>
</ul>

<p>Desuden udsender den udgivelsesansvarlige generelle statusrapporter på 
<a href="https://lists.debian.org/debian-devel-announce/">postlisten 
debian-devel-announce</a>.</p>
</if-stable-release>
