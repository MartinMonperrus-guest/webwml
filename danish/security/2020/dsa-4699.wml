#use wml::debian::translation-check translation="d948bd9589feece12bc709aa1b03a99f72526fc6" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i  Linux-kernen, hvilke kunne føre til en
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3016">CVE-2019-3016</a>

    <p>Man opdagede at KVM-implementeringen for x86 ikke altid udførte
    TLB-flush, når der er behov for det, hvis den paravirtualiserede
    TLB-flush var aktiveret.  Det kunne føre til afsløring af følsomme
    oplysninger indenfor en gæste-VM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19462">CVE-2019-19462</a>

    <p>Værktøjet syzbot fandt en manglende fejlkontrol i
    <q>relay</q>-biblioteket, der anvendes til at implementere forskellige filer
    under debugfs.  En lokal bruger, med rettigheder til at tilgå debugfs, kunne
    udnytte fejlen til at forårsage et lammelsesangreb (nedbrud) eller muligvis
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>

    <p>Efterforskere ved VU Amsterdam opdagede at på nogle Intel-CPU'er, som
    understøtter RDRAND- og RDSEED-instruktioner, kan en del af tilfældige
    værdier genereret af disse instruktioner blive anvendt i en senere
    spekulativ udførelse på en vilkårlig kerne i den samme fysiske CPU.
    Afhængigt af hvordan disse instruktioner anvendes af applikationer, kunne en
    lokal bruger eller VM-gæst udnytte fejlen til at få adgang til følsomme
    oplysninger, så som kryptografiske nøgler fra andre brugere eller VM'er.</p>

    <p>Sårbarheden kan afhjælpes med en microcode-opdatering, enten som en del
    af systemfirmware (BIOS) eller ved hjælp af pakken intel-microcode i Debians
    arkivsektion non-free.  I forbindelse med denne opdatering rapporterer vi kun
    om sårbarheden og mulighed for at deaktivere afhjælpelsen, hvis den ikke er
    nødvendig.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10711">CVE-2020-10711</a>

    <p>Matthew Sheets rapporterede om problemer med NULL-pointerdereferencer i
    undersystemet SELinux mens der modtages CIPSO-pakker med null-kategori.  En
    fjernangriber kunne drage nytte af fejlen til at forårsage et
    lammelsesangreb (nedbrud).  Bemærk at dette problem ikke påvirker de binære
    pakker, som distribueres i Debian, da CONFIG_NETLABEL ikke er
    aktiveret.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10732">CVE-2020-10732</a>

    <p>En informationslækage af privat kernehukommelse til brugerrummet blev
    fundet i kernens implementering af core dumping-brugerrumprocesser.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10751">CVE-2020-10751</a>

    <p>Dmitry Vyukov rapporterede at undersystemet SELinux ikke på korrekt vis
    håndterede validering af flere meddelelser, hvilket kunne gøre det muligt
    for en priviligeret bruger at omgå SELinux' netlinkbegrænsninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10757">CVE-2020-10757</a>

    <p>Fan Yang rapporterede om en fejl i den måde, mremap håndterede DAX-hugepages,
    hvilket gjorde det muligt for en lokal bruger at forøge sine
    rettigheder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12114">CVE-2020-12114</a>

    <p>Piotr Krysiuk opdagede en kapløbstilstand mellem umount- og
    pivot_root-handlinger i filesystem-core (vfs).  En lokal bruger med en
    CAP_SYS_ADMIN-kapabilitet i ethvert brugernavnerum, kunne udnytte fejlen til
    at forårsage et lammelsesangreb (nedbrud).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12464">CVE-2020-12464</a>

    <p>Kyungtae Kim rapporterede om en kapløbstilstand i USB-core, hvilken kunne
    medføre anvendelse efter frigivelse.  Det er uklart hvordan denne fejl kan
    udnyttes, men det medføre et lammelsesangreb (nedbrud eller
    hukommelseskorruption) eller rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12768">CVE-2020-12768</a>

    <p>En fejl blev opdaget i KVM-implementeringen for AMD-processorer, hvilken
    kunne medføre en hukommelseslækage.  Sikkerhedspåvirkningen er
    uklar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12770">CVE-2020-12770</a>

    <p>Man opdagede at sg-driveren (SCSI generic) ikke på korrekt vis frigav
    interne ressourcer i en bestemt fejlsituation.  En lokal bruger med
    rettigheder til at tilgå en sg-enhed, kunne muligvis anvende fejlen til
    at forårsage et lammelsesangreb (udmattelse af ressourcer).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13143">CVE-2020-13143</a>

    <p>Kyungtae Kim rapporterede om en potentiel heap-skrivning udenfor
    grænserne i USB-gadget-undersystemet.  En lokal bruger med rettigheder til
    at skrive til gadgetopsætningsfilsystemet, kunne anvende fejlen til at
    forårsage et lammelsesangreb (nedbrud eller hukommelseskorruption) eller
    potentielt rettighedsforøgelse.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i version
4.19.118-2+deb10u1.  Denne version retter også nogle relaterede fejl, som ikke
har deres egne CVE-ID'er, samt en regression i &lt;linux/swab.h&gt;-UAPI-headeren,
opstået i den tidligere punktopdatering (fejl nummer 960271).</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4699.data"
