#use wml::debian::mainpage title="Det universelle styresystem" GEN_TIME="yes"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="db28b03ffeea30dd379cb4120e6f83d0e85722d8"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<span class="download"><a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">Hent Debian <current_release_short><em>(64 bit-netværksinstallering til pc)</em></a> </span>

<div id="splash" style="text-align: center;">
        <h1>Debian</h1>
</div>

<div id="intro">
    <p>Debian er et 
    <a href="intro/free">frit</a> styresystem (OS, operativsystem) til din 
    computer.  Et styresystem er det sæt af basale programmer og værktøjer, der 
    gør din computer i stand til at fungere.</p>

    <p>Debian er mere end et styresystem: der er over 
    <packages_in_stable> <a href="distrib/packages">programpakker</a> - nemme at 
    hente og installere.

    <a href="intro/about">Læs mere ...</a></p>
</div>

<hometoc/>

<p class="infobar">Den <a href="releases/stable/">seneste stabile udgave af 
Debian</a> er <current_release_short>.  Den seneste opdatering til denne udgave 
blev frigivet den <current_release_date>.  Læs mere om <a href="releases/">\
tilgængelige versioner af Debian</a>.</p>


<h2>Om at komme i gang</h2>

<p>Benyt navigeringsbjælken øverst på siden, for at tilgå mere indhold.</p>

<p>Desuden kan personer, der forstår andre sprog end engelsk, tage et kik på de 
<a href="international/">internationale</a> sider, og personer der anvender 
andre systemer end Intel x86, bør kigge på <a href="ports/">\
tilpasningssiderne</a>.</p>

<hr />

<a class="rss_logo" href="News/news">RSS</a>
<h2>Seneste nyt</h2>

<:= get_recent_list('News/$(CUR_YEAR)', '6', '$(ENGLISHDIR)', '', '\d+\w*' ) :>

<p>Ældre nyheder findes på <a href="$(HOME)/News/">nyhedssiden</a>. 
Hvis du er interesseret i at modtage en e-mail, når nyheder om Debian bliver 
udsendt, kan du tilmelde dig postlisten 
<a href="MailingLists/debian-announce">debian-announce</a>.</p>

<hr />

<h2>Sikkerhedsbulletiner</h2>
<a class="rss_logo" href="security/dsa">RSS</a>
<:= get_recent_list ('security/2w', '10', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)' ) :>

<p>Ældre sikkerhedsbulletiner findes på <a href="$(HOME)/security/">\
sikkerhedssiden</a>.</p>

<p>Hvis du er interesseret i at modtage sikkerhedsbulletiner så snart de 
annonceres, kan du tilmelde dig postlisten 
<a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a>.</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian News" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Debian Project News" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Debians sikkerhedsbulletiner (kun overskrifter)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debians sikkerhedsbulletiner (opsummeringer)" href="security/dsa-long">
:#rss#}
