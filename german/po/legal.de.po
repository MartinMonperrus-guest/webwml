# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Dr. Tobias Quathamer <toddy@debian.org>, 2004, 2005, 2006.
# Helge Kreutzmann <debian@helgefjell.de>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml templates\n"
"PO-Revision-Date: 2008-06-29 22:12+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: Debian l10n German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.10.2\n"

#: ../../english/template/debian/legal.wml:15
msgid "License Information"
msgstr "Lizenzinformationen"

#: ../../english/template/debian/legal.wml:19
msgid "DLS Index"
msgstr "DLS-Index"

#: ../../english/template/debian/legal.wml:23
msgid "DFSG"
msgstr "DFSG"

#: ../../english/template/debian/legal.wml:27
msgid "DFSG FAQ"
msgstr "DFSG FAQ"

#: ../../english/template/debian/legal.wml:31
msgid "Debian-Legal Archive"
msgstr "Debian-Legal-Archiv"

#. title string without version, on the form "dls-xxx - license name: status"
#: ../../english/template/debian/legal.wml:49
msgid "%s  &ndash; %s: %s"
msgstr "%s &ndash; %s: %s"

#. title string with version, on the form "dls-xxx - license name, version: status"
#: ../../english/template/debian/legal.wml:52
msgid "%s  &ndash; %s, Version %s: %s"
msgstr "%s &ndash; %s, Version %s: %s"

#: ../../english/template/debian/legal.wml:59
msgid "Date published"
msgstr "Datum der Veröffentlichung"

#: ../../english/template/debian/legal.wml:61
msgid "License"
msgstr "Lizenz"

#: ../../english/template/debian/legal.wml:64
msgid "Version"
msgstr "Version"

#: ../../english/template/debian/legal.wml:66
msgid "Summary"
msgstr "Bewertung"

#: ../../english/template/debian/legal.wml:70
msgid "Justification"
msgstr "Begründung"

#: ../../english/template/debian/legal.wml:72
msgid "Discussion"
msgstr "Diskussion"

#: ../../english/template/debian/legal.wml:74
msgid "Original Summary"
msgstr "Ursprüngliche Bewertung"

#: ../../english/template/debian/legal.wml:76
msgid ""
"The original summary by <summary-author/> can be found in the <a href="
"\"<summary-url/>\">list archives</a>."
msgstr ""
"Die ursprüngliche Bewertung durch <summary-author/> kann in den <a href="
"\"<summary-url/>\">Listenarchiven</a> gefunden werden."

#: ../../english/template/debian/legal.wml:77
msgid "This summary was prepared by <summary-author/>."
msgstr "Diese Bewertung wurde von <summary-author/> erstellt."

#: ../../english/template/debian/legal.wml:80
msgid "License text (translated)"
msgstr "Lizenztext (Übersetzung)"

#: ../../english/template/debian/legal.wml:83
msgid "License text"
msgstr "Lizenztext"

#: ../../english/template/debian/legal_tags.wml:6
msgid "free"
msgstr "frei"

#: ../../english/template/debian/legal_tags.wml:7
msgid "non-free"
msgstr "unfrei"

#: ../../english/template/debian/legal_tags.wml:8
msgid "not redistributable"
msgstr "nicht weitervertreibbar"

#. For the use in headlines, see legal/licenses/byclass.wml
#: ../../english/template/debian/legal_tags.wml:12
msgid "Free"
msgstr "Frei"

#: ../../english/template/debian/legal_tags.wml:13
msgid "Non-Free"
msgstr "Unfrei"

#: ../../english/template/debian/legal_tags.wml:14
msgid "Not Redistributable"
msgstr "Nicht Weitervertreibbar"

#: ../../english/template/debian/legal_tags.wml:27
msgid ""
"See the <a href=\"./\">license information</a> page for an overview of the "
"Debian License Summaries (DLS)."
msgstr ""
"Einen Überblick über die »Debian License Summaries« (DLS, Debian "
"Lizenzbewertungen) finden Sie auf der <a href=\"./"
"\">Lizenzinformationsseite</a>."
