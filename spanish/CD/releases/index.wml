#use wml::debian::cdimage title="Información de la publicación de imágenes de CD de Debian" BARETITLE=true
#use wml::debian::translation-check translation="981a8578736a09678fc193cac33fe0f2ff036381"

<p>Esta página contiene información histórica de último minuto sobre las imágenes oficiales de Debian.</p>

<p>La información sobre problemas de instalación de versiones posteriores a las que se listan a continuación está disponible en la página con la
<q>información de instalación</q> de cada versión.
Para la versión «estable» actual, esta información se puede encontrar 
<a href="$(HOME)/releases/stable/debian-installer/">aquí</a>.</p>

<p>
<strong>La siguiente lista ya no se actualiza.</strong>
</p>

<hrline />

<dl>
# ------------------------------------------------------------
  <release-notes title="Official Debian 4.0 rev2" version="4.0 rev2">
    <p>Sin problemas conocidos.</p>
  </release-notes>
# ------------------------------------------------------------
  <release-notes title="Official Debian 4.0 rev1" version="4.0 rev1">
    <p>Sin problemas conocidos.</p>
  </release-notes>
# ------------------------------------------------------------
 <release-notes title="Official Debian 4.0 rev0" version="4.0 rev0">
 
<p>Si instala desde CD-ROM o DVD y elige usar una réplica en red
durante la instalación, puede que tras la instalación haya líneas
refiriéndose a <q>sarge</q> en lugar de a <q>etch</q> en el archivo
<tt>/etc/apt/sources.list</tt>.<br />
Esto solo puede suceder si la réplica que ha elegido no está actualizada
y todavía tiene sarge como publicación estable. Se recomienda que
quienes instalen etch desde CD/DVD poco después de la 
publicación comprueben su archivo <tt>sources.list</tt> tras la
instalación y reemplacen <q>sarge</q> por <q>etch</q> si es necesario.</p>

 </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev6" version="3.1 rev6a">
    <p>Sin problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev6" version="3.1 rev6">
    <p>Se descubrió un fallo en las imágenes de CD/DVD de instalación de 
    la versión 3.1r6:
    las instalaciones fallarían inmediatamente por la falta de un enlace 
    simbólico a <q>oldstable</q>. Sin embargo, las imágenes de CD/DVD de actualización 
    funcionan correctamente.</p>
    <p>Este problema se resolvió en las imágenes 3.1r6a.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev5" version="3.1 rev5">

    <p>Durante la instalación en arquitecturas i386, hppa, ia64 y
    s390, el instalador puede seleccionar un núcleo incorrecto para el
    sistema.<br />
    Puede sortear este problema arrancando el instalador con el parámetro
    <tt>debconf/priority=medium</tt>. Esto hará que se muestre una lista con
    todos los núcleos disponibles, de la que puede elegir manualmente el 
    apropiado para su sistema.</p>

    <p>Pedimos disculpas por cualquier inconveniente que esto pueda causar,
      esperamos corregirlo en la r6.</p>
  </release-notes>
# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev4" version="3.1 rev4">
    <p>Sin problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev3" version="3.1 rev3">

    <p>El primer CD contiene archivos extra que se deberían haber borrado,
       pero se olvidó hacerlo cuando se publicó 3.1r3. Esto causa dos 
       problemas:</p>

    <ul>
      <li>El núcleo que se instala usando estas imágenes, en algunos 
       casos, no es la última versión del núcleo incluida en el CD, sino
       la anterior. Actualice el núcleo específicamente tras 
       la instalación y todo debería ir bien.</li>

      <li>Como los archivos extra del primer CD ocupan espacio extra,
       alguna de las tareas normales de la instalación no caben ahí.
       Si quiere instalar todas las tareas sólo desde CD, necesitará
       usar el segundo CD también.</li>
    </ul>

    <p>Pedimos disculpas por cualquier problema que esto pueda ocasionarle;
       esperamos arreglarlo en la r4. Las imágenes de DVD y las instalaciones
       por red no deberían verse afectadas.</p>

  </release-notes>

# ------------------------------------------------------------
   <release-notes title="Debian 3.1 rev2 oficial" version="3.1 rev2">
    <p>Sin problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 3.1 rev1 oficial" version="3.1 rev1">
    <p>Sin problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 3.1 rev0a oficial" version="3.1 rev0a">
    <p>El archivo README del CD indica que esta es una beta no oficial.
    El README está mal, <em>sí</em> es la publicación oficial. Sentimos
    la confusión.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 3.1 rev0 oficial" version="3.1 rev0">
    <p>La instalación a partir de estas imágenes 
    <a href="https://lists.debian.org/debian-devel-announce/2005/06/msg00003.html">
    colocará una línea incorrecta para las actualizaciones de seguridad en el
    /etc/apt/sources.list</a>.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 3.0 rev6 oficial" version="3.0 rev6">
    <p>Sin problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 3.0 rev5 oficial" version="3.0 rev5">
    <p>Sin problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 3.0 rev4 oficial" version="3.0 rev4">
    <p>Sin problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 3.0 rev3 oficial" version="3.0 rev3">
    <p>Sin problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 3.0 rev2 oficial" version="3.0 rev2">
    <p>Sin problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 3.0 rev1 oficial" version="3.0 rev1">
    <p>Sin problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 3.0 rev0 oficial" version="3.0 rev0">
    <p>El README que encontrará en el CD indica que esta es una beta no
    oficial. El README está equivocado, ya que esta <em>es</em> la
    distribución oficial en CD. Sentimos la confusión.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 2.2 rev7 oficial" version="2.2 rev7">
    <p>No hay problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 2.2 rev6 oficial" version="2.2 rev6">
    <p>No hay problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Debian 2.2 rev5 oficial" version="2.2 rev5">
    <p>No hay problemas conocidos.</p>
  </release-notes>

# ------------------------------------------------------------
  <dt><strong>Debian 2.2 rev4 oficial y rev4.1 oficial</strong></dt>
    <dd>\
    <p>Los CD originales para la arquitectura powerpc no son
    arrancables. Se regeneraron, y la versión resultante fue
    la 2.2rev4.1, que no se diferencia en nada de la 2.2rev4
    salvo para powerpc.</p>\
    </dd>
# ------------------------------------------------------------
  <dt><strong>Debian 2.2 rev3 oficial</strong></dt>
    <dd>
    <p>Se ha informado de que los portátiles de Toshiba tienen
    problemas para arrancar desde los CD porque su BIOS
    <a href="https://lists.debian.org/debian-devel-0104/msg01326.html">no
    puede gestionar</a> imágenes de arranque mayores de 2,88 MB.
    La manera más fácil de comenzar la instalación de Debian en estas
    máquinas es lanzando <tt>boot.bat</tt> desde el directorio
    <tt>install/</tt> del CD Binary-1.</p>

    <p>No todos los paquetes de la sección <q>contrib</q> están incluidos en
    los CD, porque dependen de paquetes non-free (no libres) que tampoco
    están en los CD.</p>

    <p>Los problemas con el controlador de ratón en modo texto <tt>gpm</tt>
    aún no se han resuelto. Vea más abajo la sección <q>2.2 rev0</q> para
    más información.</p>

  </dd>
# ------------------------------------------------------------

  <dt><strong>Debian 2.2 rev2 oficial</strong></dt>
  <dd>
    <p>No todos los paquetes de la sección <q>contrib</q> están incluidos en
    los CD, porque dependen de paquetes non-free (no libres) que tampoco
    están en los CD.</p>

    <p>Los problemas con el controlador de ratón en modo texto <tt>gpm</tt>
    aún no se han resuelto. Vea más abajo la sección <q>2.2 rev0</q> para
    más información.</p>
  </dd>
# ------------------------------------------------------------

  <dt><strong>Debian 2.2 rev1 oficial</strong></dt>
  <dd><p>No se crearon imágenes de CD para la versión 2.2 
  rev1.</p></dd>
# ------------------------------------------------------------

  <dt><strong>Debian 2.2 rev0 oficial</strong></dt>
    <dd>
    <p>No todos los paquetes de la sección <q>contrib</q> están incluidos en
    los CD, porque dependen de paquetes non-free (no libres) que tampoco
    están en los CD.</p>

    <p><strong>i386</strong>: Hay algunos problemas con el controlador de
    ratón en modo texto <tt>gpm</tt> cuando se ejecuta el sistema de
    ventanas X. La forma más fácil de solucionarlo es borrar la línea
    <tt>repeat_type=<em>&lt;algo&gt;</em></tt> de
    <tt>/etc/gpm.conf</tt>, ejecutar <q><tt>/etc/init.d/gpm&nbsp;restart</tt></q>
    y reiniciar luego X. Otras soluciones son posibles, pregunte al
    <a href="mailto:gpm@packages.debian.org">responsable de gpm</a> para
    que le proporcione más documentación.</p>

    <p><strong>i386</strong>: La imagen disponible previamente del CD
    Binary-2 tenía un problema en un bit que impedía la
    instalación del paquete <q><tt>pdksh</tt></q>. Reparar su propia copia de la
    imagen es muy fácil con el programa
    <a href="https://cdimage.debian.org/~costar/correct_cds/correct-i386-2.c">correct-i386-2.c</a>.</p>

    <p>Gracias a <a href="mailto:kteague@sprocket.dhis.net">Ken Teague</a>
    también disponemos de una
    <a href="https://cdimage.debian.org/~costar/correct_cds/correct-i386-2.zip">versión
    para Windows</a> precompilada; uso: extraiga el <tt>.zip</tt> en el directorio en
    el que esté el archivo de imagen <tt>binary-i386-2.iso</tt>, luego en
    una ventana de DOS vaya a ese directorio y teclee la orden 
    <q><tt>correct-i386-2 binary-i386-2.iso</tt></q>.</p>

    <p>Si tiene un CD-ROM, que obviamente no puede reparar, la manera más
    fácil de obtener <tt>pdksh</tt> es 
    <a href="http://archive.debian.org/debian/dists/potato/main/binary-i386/shells/pdksh_5.2.14-1.deb">descargarlo</a>
    (212 KB) e instalarlo con <q><tt>dpkg -i pdksh_5.2.14-1.deb</tt></q>.
    Pero también puede copiar el archivo del CD a un directorio temporal y
    usar el mismo programa
    <a href="https://cdimage.debian.org/~costar/correct_cds/correct-i386-2.c">correct-i386-2.c</a>,
    aunque entonces debería modificarlo para que <tt>POS</tt> sea
    <tt>0x64de</tt>.</p>

    <p><strong>PowerPC</strong>: Las imágenes de los CD Binary-1_NONUS
    y Binary-3 están también afectados por la plaga de problemas de un bit,
    que impide que se puedan instalar <q><tt>smbfs</tt></q> y
    <q><tt>gimp-manual</tt></q>, respectivamente. Las versiones reparadas se
    están propagando (lentamente) por las réplicas, pero
    puede reparar sus propias imágenes muy fácilmente con los programas
    <a href="https://cdimage.debian.org/~costar/correct_cds/correct-powerpc-1_NONUS.c">correct-powerpc-1_NONUS.c</a>
    y 
    <a href="https://cdimage.debian.org/~costar/correct_cds/correct-powerpc-3.c">correct-powerpc-3.c</a>.
    Ambos también contienen información sobre la reparación de los paquetes
    afectados individualmente al copiarlos desde el CD (que es especialmente
    útil para <q><tt>gimp-manual</tt></q>, cuyo tamaño es de 15 MB).</p>

    <p><strong>Sparc</strong>: Los CD de la versión 2.2 rev0 tienen
    un problema al arrancar desde el CD Binary-1. Esto ha sido resuelto
    en las versión 2.2 rev0a (o 2.2_rev0_CDa) de los CD de sparc.</p>

    <p>Sparc: Si X no se inicia correctamente y el mensaje de error
    menciona el ratón y <em>no</em> está ejecutando el controlador de
    ratón en modo texto <tt>gpm</tt>, puede ayudarle
    <q><tt>rm -f /dev/gpmdata</tt></q>.</p>

    <p><strong>Alpha</strong>: Los CD de 2.2 rev0 pueden experimentar
    arranques problemáticos desde el CD Binary-1. Para solucionar
    esto, arranque con <q><tt>-flags i</tt></q> y, a continuación, introduzca en
    el prompt de aboot:
    <br />
      <tt>&nbsp;&nbsp;&nbsp;&nbsp;aboot&gt; b /linux
      initrd=/boot/root.bin root=/dev/ram</tt>
    <br />
    Este problema se ha corregido en la versión 2.2 rev0a (o 2.2_rev0_CDa)
    de los CD de alpha.</p>
  </dd>
# ------------------------------------------------------------

  <dt><strong>Potato test-cycle-3</strong></dt>
    <dd><p>El controlador de ratón en modo texto <tt>gpm</tt>
    tiene algunos prolemas. <strong>No</strong> debería ejecutar el programa
    <tt>mouse-test</tt>, y tendrá que ejecutar <q><tt>/etc/init.d/gpm&nbsp;stop</tt></q>
    antes de poder usar el ratón en X.</p></dd>
# ------------------------------------------------------------

  <dt><strong>Potato test-cycle-2</strong></dt>
    <dd><p>El controlador de ratón en modo texto <tt>gpm</tt>
    tiene algunos prolemas. <strong>No</strong> debería ejecutar el programa
    <tt>mouse-test</tt>, y tendrá que ejecutar <q><tt>/etc/init.d/gpm&nbsp;stop</tt></q>
    antes de poder usar el ratón en X.</p></dd>
# ------------------------------------------------------------

  <dt><strong>Potato test-cycle-1</strong></dt>
    <dd><p>No hay información relevante.</p></dd>

</dl>
