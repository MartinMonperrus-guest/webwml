#use wml::debian::template title="Debian 10 -- Erratas" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="cbc6ad51e9166766652bfb4260e76ff1f2756c25"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problemas conocidos</toc-add-entry>
<toc-add-entry name="security">Problemas de seguridad</toc-add-entry>

<p>El equipo de seguridad de Debian publica actualizaciones de paquetes de la versión «estable»
en los cuales ha identificado problemas relacionados con la seguridad. Por favor, consulte las
<a href="$(HOME)/security/">páginas de seguridad</a> para información sobre
cualquier problema de seguridad identificado en <q>buster</q>.</p>

<p>Si usa APT, agregue la siguiente línea en <tt>/etc/apt/sources.list</tt>
para tener acceso a las últimas actualizaciones de seguridad:</p>

<pre>
  deb http://security.debian.org/ buster/updates main contrib non-free
</pre>

<p>Después, ejecute <kbd>apt update</kbd> seguido de
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Versiones</toc-add-entry>

<p>A veces, en el caso de varios problemas críticos o actualizaciones de seguridad, la
distribución publicada se actualiza. Generalmente, esto se indica con un nuevo número de versión («point
release» en inglés).</p>

<ul>
  <li>La primera versión, 10.1, se publicó el
      <a href="$(HOME)/News/2019/20190907">7 de septiembre de 2019</a>.</li>
  <li>La segunda versión, 10.2, se publicó el
      <a href="$(HOME)/News/2019/20191116">16 de noviembre de 2019</a>.</li>
 <li>La tercera versión, 10.3, se publicó el
      <a href="$(HOME)/News/2020/20200208">8 de febrero de 2020</a>.</li>
 <li>La cuarta versión, 10.4, se publicó el
      <a href="$(HOME)/News/2020/20200509">9 de mayo de 2020</a>.</li>
 <li>La quinta versión, 10.5, se publicó el
      <a href="$(HOME)/News/2020/20200801">1 de agosto de 2020</a>.</li>
 <li>La sexta versión, 10.6, se publicó el
      <a href="$(HOME)/News/2020/20200926">26 de septiembre de 2020</a>.</li>
</ul>

<ifeq <current_release_buster> 10.0 "

<p>Aún no hay versiones posteriores para Debian 10.</p>" "

<p>Vea el <a
href="http://http.us.debian.org/debian/dists/buster/ChangeLog">\
registro de cambios («ChangeLog»)</a>
para detalles sobre los cambios entre 10 y <current_release_buster/>.</p>"/>


<p>Las correcciones para la distribución «estable» publicada pasan un
período de pruebas extendido antes de ser aceptadas en el archivo.
Sin embargo, estas correcciones están disponibles en el directorio
<a href="http://ftp.debian.org/debian/dists/buster-proposed-updates/">\
dists/buster-proposed-updates</a> de cualquier réplica del archivo de
Debian.</p>

<p>Si usa APT para actualizar los paquetes, puede instalar
las actualizaciones propuestas agregando la siguiente línea en
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# adiciones propuestas para una versión de Debian 10
  deb http://deb.debian.org/debian buster-proposed-updates main contrib non-free
</pre>

<p>Después, ejecute <kbd>apt update</kbd> seguido de
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Sistema de instalación</toc-add-entry>

<p>
Para información sobre erratas y actualizaciones del sistema de instalación, vea
la página de <a href="debian-installer/">información de instalación</a>.
</p>
