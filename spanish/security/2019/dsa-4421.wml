#use wml::debian::translation-check translation="5357ead7bb298e3857977fea7b0087543c6072b3"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el navegador web Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5787">CVE-2019-5787</a>

    <p>Zhe Jin descubrió un problema de «uso tras liberar».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5788">CVE-2019-5788</a>

    <p>Mark Brand descubrió un problema de «uso tras liberar» en la implementación
    de FileAPI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5789">CVE-2019-5789</a>

    <p>Mark Brand descubrió un problema de «uso tras liberar» en la implementación
    de WebMIDI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5790">CVE-2019-5790</a>

    <p>Dimitri Fourny descubrió un problema de desbordamiento de memoria en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5791">CVE-2019-5791</a>

    <p>Choongwoo Han descubrió un error de confusión de tipo en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5792">CVE-2019-5792</a>

    <p>pdknsk descubrió un problema de desbordamiento de entero en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5793">CVE-2019-5793</a>

    <p>Jun Kokatsu descubrió un problema de permisos en la implementación
    de Extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5794">CVE-2019-5794</a>

    <p>Juno Im, de Theori, descubrió un problema de suplantación de la interfaz de usuario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5795">CVE-2019-5795</a>

    <p>pdknsk descubrió un problema de desbordamiento de entero en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5796">CVE-2019-5796</a>

    <p>Mark Brand descubrió una condición de carrera en la implementación de Extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5797">CVE-2019-5797</a>

    <p>Mark Brand descubrió una condición de carrera en la implementación de DOMStorage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5798">CVE-2019-5798</a>

    <p>Tran Tien Hung descubrió un problema de lectura fuera de límites en la biblioteca skia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5799">CVE-2019-5799</a>

    <p>sohalt descubrió una manera de sortear la política de seguridad del contenido.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5800">CVE-2019-5800</a>

    <p>Jun Kokatsu descubrió una manera de sortear la política de seguridad del contenido.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5802">CVE-2019-5802</a>

    <p>Ronni Skansing descubrió un problema de suplantación de la interfaz de usuario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5803">CVE-2019-5803</a>

    <p>Andrew Comminos descubrió una manera de sortear la política de seguridad del contenido.</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 73.0.3683.75-1~deb9u1.</p>

<p>Le recomendamos que actualice los paquetes de chromium.</p>

<p>Para información detallada sobre el estado de seguridad de chromium, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/chromium">https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4421.data"
