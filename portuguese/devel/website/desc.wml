#use wml::debian::template title="Como o www.debian.org é feito"
#use wml::debian::toc
#use wml::debian::translation-check translation="6d9ec7201dcfa229e0cdf7f93c34b3d4a8509ebe" maintainer="Thiago Pezzo (Tico)"


<p>A <em>&ldquo;árvore web&rdquo;</em> do Debian, uma coleção de
diretórios e arquivos que compreendem nosso web site, está localizada no
diretório <tt>/org/www.debian.org/www</tt> em www-master.debian.org. A
maioria das páginas são arquivos HTML normais e estáticos (ou seja, não produzidos
dinamicamente com CGI ou scripts PHP) para facilitar o espelhamento.

<p>O site é gerado em um dos três processos:
<ul>
  <li>a maior parte é gerada usando WML, a partir do
      <a href="$(DEVEL)/website/using_git">repositório git <q>webwml</q></a>
  <li>a documentação é gerada usando DocBookXML (o uso de SGML DebianDoc está
      sendo eliminado), a partir do
      <a href="$(DOC)/vcs">repositório git <q>ddp</q></a>; ou
      usando <a href="#scripts">scripts cron</a> dos pacotes Debian
      correspondentes
  <li>partes do site são geradas usando scripts de outras fontes,
    como as páginas de inscrição e remoção das listas de discussão.
</ul>

<p>Uma atualização automática (do repositório git e de outras fontes da árvore
web) é feita seis vezes por dia.

<p>Se você quer contribuir para o site, <strong>não</strong>
adicione ou edite arquivos diretamente no diretório <code>www/</code>.
Contate <a href="mailto:webmaster@debian.org">os(as) webmasters</a>
primeiro.

<p>Todos os arquivos e diretórios pertencem ao grupo debwww e podem ser
escritos por este grupo, assim a equipe web pode modificar os arquivos nos
diretórios web. O modo 2775 nos diretórios significa que qualquer arquivo
criado naqueles diretórios vão herdar o grupo - neste caso debwww. Espera-se
que todos(as) no grupo debwww configurem '<code>umask 002</code>' de modo que os
arquivos sejam criados com permissões de escrita de grupo.

<toc-display />

<hrline />


<toc-add-entry name="look">Aparência &amp; comportamento</toc-add-entry>

<p>Nós damos às páginas a mesma aparência e comportamento através do uso do
<a href="https://packages.debian.org/unstable/web/wml">WML</a>
para todos os detalhes de adição de cabeçalhos e rodapés.
Embora uma página .wml possa parecer HTML à primeira vista, HTML é somente
um dos tipos de informação extra que podem ser usados em .wml. Depois que
o WML termina de rodar seus vários filtros sobre um arquivo, o produto
final é HTML puro. Para dar uma ideia do poder do WML, você pode
incluir código Perl em uma página para permitir-lhe fazer quase qualquer coisa.

<p>Note, no entanto, que o WML checa (e corrige automagicamente algumas
vezes) apenas a validade mais básica de seu código HTML. Você deve
instalar
<a href="https://packages.debian.org/unstable/web/weblint">weblint</a>
e/ou
<a href="https://packages.debian.org/unstable/web/tidy">tidy</a>
para validar seu código HTML.

<p>Nossas páginas web atualmente seguem o padrão
<a href="http://www.w3.org/TR/html4/">HTML 4.01 Strict</a>.

<p>Todos(as) que estejam trabalhando em muitas páginas deveriam instalar o WML
de modo que possam testar e se certificar de que o resultado é
aquele que desejam. Se você está rodando Debian, pode instalar
o pacote <code>wml</code> facilmente. Leia as páginas em
<a href="using_wml">usando WML</a> para mais informações.


<toc-add-entry name="sources">Fontes</toc-add-entry>

<p>Os arquivos-fonte para as páginas web estão armazenadas no git. O git é um
sistema de controle de versão que nos permite manter um histórico (log) de
o quê, quem, quando e porque as alterações ocorreram. Ele é um
jeito seguro de controlar a edição concorrente de arquivos-fonte
por muitos(as) autores(as), o que é crucial para nós porque o
time web do Debian é grande em tamanho.

<p>Se você não está familiarizado com este software, provavelmente desejará ler
as páginas <a href="using_git">usando o git</a>.

<p>O primeiro diretório webwml no repositório git contém diretórios
nomeados segundo os idiomas nos quais o site está disponível, dois makefiles
e vários scripts. Os nomes dos diretórios de traduções devem estar
em inglês e minúsculas (ou seja, "german" em vez de "Deutsch").

<p>O mais importante dos dois makefiles é o Makefile.common que, como
o nome diz, contém algumas regras comuns que são aplicadas ao incluir
este arquivo nos outros makefiles.

<p>Cada um dos diretórios de idioma contém makefiles, arquivos-fonte WML e
subdiretórios. Os nomes de arquivos e diretórios não são alterados,
mantendo os links corretos para todos os idiomas. Os diretórios
também podem conter arquivos .wmlrc que contêm informações úteis para o WML.

<p>O diretório webwml/english/template contém arquivos WML especiais que
nós chamamos de templates (modelos), porque eles podem ser referenciados por
todos os outros arquivos usando o comando <code>#use</code>.

<p>Para que as alterações nos templates se propaguem para os arquivos
que os utilizam, os arquivos têm dependências via makefile. Uma vez
que a grande maioria dos arquivos usa o template "template", tendo a
linha "<code>#use wml::debian::template</code>" no topo, a dependência
genérica (aquela para todos os arquivos) é esse mesmo template. Claro que
há exceções para essa regra.


<toc-add-entry name="scripts">Scripts</toc-add-entry>

<p>Os scripts são escritos principalmente em shell ou Perl. Alguns
deles são avulsos e alguns estão integrados em arquivos-fonte WML.</p>

<p>Os fontes para os principais scripts de reconstrução do www-master
estão no
<a href="https://salsa.debian.org/webmaster-team/cron.git">repositório Git
debwww/cron</a>.
</p>

<p>Os fontes para os scripts de reconstrução do packages.debian.org
estão no
<a href="https://salsa.debian.org/webmaster-team/packages">repositório Git
webwml/packages</a>.</p>


<toc-add-entry name="help">Como ajudar</toc-add-entry>

<p>Convidamos todos(as) os(as) interessados(as) em nos ajudar a fazer o web site
do Debian tão bom quanto ele possa ser. Se você tem informações valiosas
relacionadas ao Debian que você acredita que estão faltando nas nossas páginas,
<a href="mailto:debian-www@lists.debian.org">compartilhe-as conosco</a> e nós
faremos com que elas sejam incluídas.

<p>Nós sempre precisamos de ajuda com o design das páginas (a respeito de
gráficos e layouts), e para manter nosso HTML limpo também. Nós rodamos
os seguintes testes em todo o site regularmente:</p>

<ul>
  <li><a href="https://www-master.debian.org/build-logs/urlcheck/">verificação de URL</a>
  <li><a href="https://www-master.debian.org/build-logs/validate/">wdg-html-validator</a>
  <li><a href="https://www-master.debian.org/build-logs/tidy/">tidy</a>
</ul>

<p>A ajuda é sempre bem-vinda na leitura dos logs acima e na correção dos
problemas.</p>

<p>Os logs atualizados de construção do site podem ser encontrados em
<url "https://www-master.debian.org/build-logs/">.</p>

<p>Se você é fluente em inglês, nós gostaríamos que você revisse as nossas
páginas e <a href="mailto:debian-www@lists.debian.org">nos</a> relatasse
todos os erros.

<p>Se você fala outro idioma, você pode nos ajudar a traduzir as páginas para
o seu idioma. Se alguma tradução já foi feita, mas tem algum problema, olhe a
lista de <a href="translation_coordinators">coordenadores(as) de tradução</a> e
fale com o(a) líder do seu idioma sobre corrigi-lo. Se você gostaria de traduzir
páginas, veja a página sobre <a href="translating">esse tópico</a> para mais
informações.

<p>Há também um <a href="todo">arquivo TODO</a>, dê uma olhada.</p>


<toc-add-entry name="nohelp">Como <strong>não</strong> ajudar</toc-add-entry>

<p><em>[P] Eu quero adicionar um <var>recurso web extravagante</var> em
www.debian.org, eu posso?</em>

<p>[R] Não. Nós queremos que o www.debian.org seja tão acessível quanto
possível, assim sendo:
<ul>
    <li>sem "extensões" específicas de navegador.
    <li>uso não baseado em imagens. Imagens podem ser usadas para clarificar,
        mas as informações em www.debian.org devem continuar acessíveis
        através de um navegador somente texto, como o lynx.
</ul>

<p><em>[P] Eu tenho esta ideia legal. Você pode habilitar FOO no
HTTPD do www.debian.org, por favor?</em>

<p>[R] Não. Nós queremos facilitar a vida dos(as) administradores(as) que
espelham www.debian.org, assim sendo, sem recursos especiais do HTTPD.
Não, nem mesmo SSI. Uma exceção foi feita para a negociação de conteúdo,
e isto porque ela é a única maneira robusta de servir múltiplos idiomas.
