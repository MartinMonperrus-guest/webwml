#use wml::debian::template title="O Projeto debian-volatile"
#use wml::debian::release_info
#use wml::debian::translation-check translation="b8114b588961778dbd04974c1464a2f388a90c28"

<h2>debian-volatile para usuários(as) finais</h2>

<p class="important"><strong>
O projeto debian-volatile foi descontinuado com o lançamento do Debian
<q>Squeeze</q>. Veja <a
href="https://lists.debian.org/debian-volatile-announce/2012/msg00000.html">
este anúncio</a> para detalhes.
</strong></p>

<h3>O que é o debian-volatile?</h3>
<p>Alguns pacotes visam alvos que estão em constante mudanças, como filtragem de
spam e busca por vírus, e mesmo quando usam padrões de dados atualizados
eles realmente não funcionam durante o tempo inteiro de uma versão estável. O objetivo
principal do volatile é permitir que administradores(as) atualizem seus
sistemas de uma forma agradável e consistente, sem as desvantagens
de usar a versão instável (unstable), nem mesmo as provenientes dos pacotes
selecionados. Portanto a debian-volatile contém apenas mudanças nos programas
estáveis que são necessárias para mantê-los funcionais.</p>

<h3>O que é debian-volatile/sloppy?</h3>

<p>Para pacotes na seção <em>volatile</em> do repositório debian-volatile,
nós procuramos garantir que as versões mais novas não introduzam
mudanças funcionais ou exijam que o(a) administrador(a) modifique o
arquivo de configuração.</p>


<h3>Repositório</h3>
<p>O repositório oficial do debian-volatile foi salvo em <a
href="http://archive.debian.org/debian-volatile/">archive.debian.org
</a>.</p>

<p>A integração com o repositório principal aconteceu na
<q>Squeeze</q>. Por favor veja o <a
href="$(HOME)/News/2011/20110215">anúncio</a> para detalhes de como
mudar para squeeze-updates.
</p>
