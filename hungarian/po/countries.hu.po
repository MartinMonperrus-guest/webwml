msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2011-01-10 22:52+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "Egyesült Arab Emirátusok"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "Albánia"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "Örményország"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "Argentína"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "Ausztria"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "Ausztrália"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "Bosznia-Hercegovina"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "Banglades"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "Belgium"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "Bulgária"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "Brazília"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "Bahamák"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "Fehéroroszország"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "Kanada"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "Svájc"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "Chile"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "Kína"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "Kolumbia"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "Costa Rica"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "Csehország"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "Németország"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "Dánia"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "Dominikai Köztársaság"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "Algéria"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "Ecuador"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "Észtország"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "Egyiptom"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "Spanyolország"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "Etiópia"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "Finnország"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "Feröer-szigetek"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "Franciaország"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "Nagy-Britannia"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "Granada"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "Grúzia"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "Grönland"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "Görögország"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "Gvatemala"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "Hongkong"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "Honduras"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "Horvátország"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "Magyarország"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "Indonézia"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "Irán"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "Írország"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "Izrael"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "India"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "Izland"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "Olaszország"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "Jordánia"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "Japán"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "Kenya"

#: ../../english/template/debian/countries.wml:267
#, fuzzy
msgid "Kyrgyzstan"
msgstr "Kazahsztán"

#: ../../english/template/debian/countries.wml:270
msgid "Korea"
msgstr "Korea"

#: ../../english/template/debian/countries.wml:273
msgid "Kuwait"
msgstr "Kuvait"

#: ../../english/template/debian/countries.wml:276
msgid "Kazakhstan"
msgstr "Kazahsztán"

#: ../../english/template/debian/countries.wml:279
msgid "Sri Lanka"
msgstr "Sri Lanka"

#: ../../english/template/debian/countries.wml:282
msgid "Lithuania"
msgstr "Litvánia"

#: ../../english/template/debian/countries.wml:285
msgid "Luxembourg"
msgstr "Luxemburg"

#: ../../english/template/debian/countries.wml:288
msgid "Latvia"
msgstr "Lettország"

#: ../../english/template/debian/countries.wml:291
msgid "Morocco"
msgstr "Marokkó"

#: ../../english/template/debian/countries.wml:294
msgid "Moldova"
msgstr "Moldova"

#: ../../english/template/debian/countries.wml:297
msgid "Montenegro"
msgstr "Montenegró"

#: ../../english/template/debian/countries.wml:300
msgid "Madagascar"
msgstr "Madagaszkár"

#: ../../english/template/debian/countries.wml:303
msgid "Macedonia, Republic of"
msgstr "Macedón Köztársaság"

#: ../../english/template/debian/countries.wml:306
msgid "Mongolia"
msgstr "Mongólia"

#: ../../english/template/debian/countries.wml:309
msgid "Malta"
msgstr "Málta"

#: ../../english/template/debian/countries.wml:312
msgid "Mexico"
msgstr "Mexikó"

#: ../../english/template/debian/countries.wml:315
msgid "Malaysia"
msgstr "Malajzia"

#: ../../english/template/debian/countries.wml:318
msgid "New Caledonia"
msgstr "Új Kaledónia"

#: ../../english/template/debian/countries.wml:321
msgid "Nicaragua"
msgstr "Nicaragua"

#: ../../english/template/debian/countries.wml:324
msgid "Netherlands"
msgstr "Hollandia"

#: ../../english/template/debian/countries.wml:327
msgid "Norway"
msgstr "Norvégia"

#: ../../english/template/debian/countries.wml:330
msgid "New Zealand"
msgstr "Új-Zéland"

#: ../../english/template/debian/countries.wml:333
msgid "Panama"
msgstr "Panama"

#: ../../english/template/debian/countries.wml:336
msgid "Peru"
msgstr "Peru"

#: ../../english/template/debian/countries.wml:339
msgid "French Polynesia"
msgstr "Francia Polinézia"

#: ../../english/template/debian/countries.wml:342
msgid "Philippines"
msgstr "Fülöp-szigetek"

#: ../../english/template/debian/countries.wml:345
msgid "Pakistan"
msgstr "Pakisztán"

#: ../../english/template/debian/countries.wml:348
msgid "Poland"
msgstr "Lengyelország"

#: ../../english/template/debian/countries.wml:351
msgid "Portugal"
msgstr "Portugália"

#: ../../english/template/debian/countries.wml:354
msgid "Réunion"
msgstr "Réunion"

#: ../../english/template/debian/countries.wml:357
msgid "Romania"
msgstr "Románia"

#: ../../english/template/debian/countries.wml:360
msgid "Serbia"
msgstr "Szerbia"

#: ../../english/template/debian/countries.wml:363
msgid "Russia"
msgstr "Oroszország"

#: ../../english/template/debian/countries.wml:366
msgid "Saudi Arabia"
msgstr "Szaud-Arábia"

#: ../../english/template/debian/countries.wml:369
msgid "Sweden"
msgstr "Svédország"

#: ../../english/template/debian/countries.wml:372
msgid "Singapore"
msgstr "Szingapúr"

#: ../../english/template/debian/countries.wml:375
msgid "Slovenia"
msgstr "Szlovénia"

#: ../../english/template/debian/countries.wml:378
msgid "Slovakia"
msgstr "Szlovákia"

#: ../../english/template/debian/countries.wml:381
msgid "El Salvador"
msgstr "El Salvador"

#: ../../english/template/debian/countries.wml:384
msgid "Thailand"
msgstr "Thaiföld"

#: ../../english/template/debian/countries.wml:387
msgid "Tajikistan"
msgstr "Tádzsikisztán"

#: ../../english/template/debian/countries.wml:390
msgid "Tunisia"
msgstr "Tunézia"

#: ../../english/template/debian/countries.wml:393
msgid "Turkey"
msgstr "Törökország"

#: ../../english/template/debian/countries.wml:396
msgid "Taiwan"
msgstr "Tajvan"

#: ../../english/template/debian/countries.wml:399
msgid "Ukraine"
msgstr "Ukrajna"

#: ../../english/template/debian/countries.wml:402
msgid "United States"
msgstr "Egyesült Államok"

#: ../../english/template/debian/countries.wml:405
msgid "Uruguay"
msgstr "Uruguay"

#: ../../english/template/debian/countries.wml:408
msgid "Uzbekistan"
msgstr "Üzbegisztán"

#: ../../english/template/debian/countries.wml:411
msgid "Venezuela"
msgstr "Venezuela"

#: ../../english/template/debian/countries.wml:414
msgid "Vietnam"
msgstr "Vietnám"

#: ../../english/template/debian/countries.wml:417
msgid "Vanuatu"
msgstr "Vanuatu"

#: ../../english/template/debian/countries.wml:420
msgid "South Africa"
msgstr "Dél-Afrika"

#: ../../english/template/debian/countries.wml:423
msgid "Zimbabwe"
msgstr "Zimbabwe"

#~ msgid "Great Britain"
#~ msgstr "Nagy-Britannia"

#~ msgid "Yugoslavia"
#~ msgstr "Jugoszlávia"
