#use wml::debian::template title="Terméktámogatás"
#use wml::debian::translation-check translation="6838e6aa35cea0dd360ea9a9f08965ebdc8c9d50" maintainer="Szabolcs Siebenhofer"
#use wml::debian::toc

# translated by Péter Mamuzsics <zumu@mentha.hu>
# updated by Hajnalka Hegedűs <heha@inf.elte.hu>
# updated by Szabolcs Siebenhofer <the7up@gmail.com>

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

Önkéntesek közösége működteti a Debian-t és annak támogatását.

Ha ez a közösság által nyújtott támogatás nem elégíti ki az igényeidet, elolvashatod a <a href="doc/">dokumentációnkat</a> vagy megbízhatsz egy <a href="consultants/">szaktanácsadót</a>.

<toc-display/>

<toc-add-entry name="irc">Online IRC segítségnyújtás</toc-add-entry>

<p>Az <a href="http://www.irchelp.org/">IRC (Internet csevegés)</a>
lehetőséget ad arra, hogy a világ bármely pontjáról származó emberekkel
valós időben "beszélgessünk". Az <a href="https://www.oftc.net/">OFTC-n</a>
találhatóak kifejezetten Debian-os csatornák.</p>
<p>A csatlakozáshoz szükség van egy IRC-kliensre. A legnépszerűbb kliensek
az <a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
az <a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
az <a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
az <a href="https://packages.debian.org/stable/net/epic5">epic5</a> és
a <a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
ezek mindegyike
megszerezhető Debian csomagban is. Az OFTC megtalálható egy <a href="https://www.oftc.net/WebChat/">WebChat</a> interfész is, ahol lehetőséged van az IRC-hez csatlakozni böngészőből, helyi kliens telepítése nélkül.</p>

<p>A kliens telepítése után meg kell adni, hogy csatlakozzon a szerverhez.
A legtöbb kliens esetén ehhez a következőt kell beírni:</p>

<pre>
/server irc.debian.org
</pre>

<p>A kapcsolódás után csatlakozz a <code>#debian</code> csatornához. Ehhez
írd be a következőt:</p>
<pre>
/join #debian
</pre>

<p>Megjegyzés: Számos kliensnél, mint amilyen az HexChat, eltérő módon,
a grafikus felület segítségével lehet a szerverhez és a csatornákhoz
kapcsolódni.</p>

<p>Ha mindez megtörtént, akkor a <code>#debian</code> csatorna
lakóinak barátságos
csődületében találod magadat. Itt bátran felteheted a Debiannal
kapcsolatos kérdéseidet. A csatorna gyakori kérdéseit a
<url "https://wiki.debian.org/DebianIRC" /> címen találod.</p>


<p>Számos más IRC-hálózaton is tudsz csevegni a Debianról.  Az egyik legjobb
a <a href="https://freenode.net/">freenode IRC-hálózat</a>
a <kbd>chat.freenode.net</kbd> címen.</p>




<toc-add-entry name="mail_lists" href="MailingLists/">Levelezőlisták</toc-add-entry>

<p>A Debian fejlesztése az egész világot felölelő osztott
fejlesztés keretein belül történik. Ennek megfelelően a különféle kérdések
megtárgyalásának legjobb módja az e-mail.
A Debian fejlesztők és felhasználók közti párbeszéd nagy része a
levelezőlistákon bonyolódik.
<p>Ehhez több nyilvános levelezőlista is rendelkezésre áll. További
információkért nézd meg a <a href="MailingLists/">Debian
levelezőlisták</a> oldalt.
<p>Angol nyelvű támogatásért keresd a <a href="https://lists.debian.org/debian-user/">debian-user
levelező listát</a>.</p>
<p>Magyar nyelvű támogatást a <a href="http://https://lists.debian.org/debian-user-hungarian/">debian-user-hungarian</a>levelező listán találsz.</p>
<p>Természetesen vannak más levelző listák is, amelyeket a Linux
ökoszisztéma különböző aspektusainak szenteltek, de ezek nem 
Debian-specifikusak. A kedvenc keresőd használatával könnyen megtalálhatod
a céljaidnak legjobban megfelelő listát.

<toc-add-entry name="usenet">Usenet hírcsoportok</toc-add-entry>

<p>Számos <a href="#mail_lists">levelezőlistánk</a> hírcsoportként is
olvasható a <kbd>linux.debian.*</kbd> hierarchiában.  Erre használhatóak
olyan webes felületek is, mint a
<a href="https://groups.google.com/forum/">Google Groups</a>.

<toc-add-entry name="web">Weboldalak</toc-add-entry>

<h3 id="forums">Fórumok</h3>

<p>A <a href="http://hup.hu/forum/135">HUP fórumán</a> magyar nyelven tehetsz fel
kérdéseket, amire más felhasználók válaszolnak majd.</p>

<p>A <a href="http://forums.debian.net/">Debian User Forums</a> olyan
webhely, ahova Debiannal kapcsolatos kérdéseket küldhetsz; ezekre a
többi felhasználó válaszol. (angol nyelven)</p>

<toc-add-entry name="maintainers">Csomagok karbantartóinak elérése</toc-add-entry>

<p>A csomagok karbantartói kétféleképpen érhetők el. Ha egy hiba miatt
szeretnél kapcsolatba lépni egy fejlesztővel, akkor egyszerűen készíts egy
hibajelentést (erről részletesebben az alábbiakban olvashatsz). A
karbantartó megkapja a hibajelentés egy példányát.
<p>Ha egyszerűen csak kommunikálni szeretnél a karbantartóval, akkor
használhatod az egyes csomagok speciális levélcímeit. Minden
&lt;<EM>csomagnév</EM>&gt;@packages.debian.org címre küldött levél
továbbításra kerül a csomagért felelős személyhez.

<toc-add-entry name="bts" href="Bugs/">Hibakövetési rendszer</toc-add-entry>

<p>A Debian disztribúció hibakövetési rendszere részletesen
felsorolja a felhasználók és fejlesztők által jelentett hibákat. Minden
hiba kap egy számot, és mindaddig a nyilvántartásban marad, amíg valaki le
nem kezeli.
<p>Hibák jelentéséhez elovashatod a lenti hibajelentési lapot; mi a 'reportbug' Debian
csomagot javasoljuk, amely automatikusan nyilvántartásba veszi a
hibajelentéseket.
<p>A hibák beküldésével, a jelenleg aktív hibák megjelenítésével, és a
hibakövetési rendszerrel kapcsolatos általános információk a
<a href="Bugs/">hibakövetési rendszer webhelyén</a> találhatók.

<toc-add-entry name="consultants" href="consultants/">Tanácsadók</toc-add-entry>

<p>A Debian szabad szoftver, és ingyenes segítségnyújtást biztosít a
levelezési listáin. Bizonyos embereknek nincs elég idejük, vagy speciális
igényeik vannak, így hajlandóak megfizetni valakit, aki karbantartja vagy
új funkciókkal fejleszti tovább a Debian rendszerüket. Az ezzel foglalkozó
emberek és cégek listája a <a href="consultants/">tanácsadók lapon</a>
található.