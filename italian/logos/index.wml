#use wml::debian::template title="Loghi di Debian" BARETITLE=true
#include "$(ENGLISHDIR)/logos/index.data"
#use wml::debian::translation-check translation="fd23b78ec868688ad9b63dfbaed4a62b660ad125" maintainer="Giuseppe Sacco"

<p>Debian ha due loghi. Il <a href="#open-use">logo ufficiale</a> (noto
come <q>logo di libero utilizzo</q>) contiene il famoso <q>vortice</q> e
rappresenta al meglio l'identità visuale del progetto Debian. Esiste anche
un <a href="#restricted-use">logo riservato</a> usato solo dal progetto
Debian e dai suoi membri. Per fare riferimento a Debian, preferire il logo
a uso libero.</p>

<hr>

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
</colgroup>
<tr>
<th colspan="2"><a name="open-use">Logo Debian con licenza di
libero utilizzo</a></th>
</tr>
<tr>
<td>

<p>Il logo Debian di libero utilizzo è disponibile in due varianti: con
e senza l'etichetta &ldquo;Debian&rdquo;.</p>

<p>I loghi Debian  di libero utilizzo sono Copyright (c) 1999 di <a
href="https://www.spi-inc.org/">Software in the Public Interest, Inc.</a>,
e rilasciati secondo i termini della <a
href="https://www.gnu.org/copyleft/lgpl.html">GNU Lesser General Public
License</a>, versione 3 o successiva revisione oppure, a vostro giudizio,
secondo la <a href="https://creativecommons.org/licenses/by-sa/3.0/">Creative
Commons Attribution-ShareAlike 3.0 Unported License</a>.</p>

<p>Nota: ci farebbe piacere che l'immagine sia un link a <a
href="$(HOME)">https://www.debian.org/</a> se usate il logo in una
pagina web.</p>
</td>
<td>
<openlogotable>
</td>
</tr>
</table>

<hr />

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
</colgroup>
<tr>
<th colspan="2"><a name="restricted-use">Logo Debian per
usi ufficiali</a></th>
</tr>
<tr>
<td>

<h3>Licenza del logo riservato Debian per usi ufficiali</h3>

<p>Copyright (c) 1999 Software in the Public Interest</p>
<ol>
	<li>Questo logo può essere usato solo se:
	<ul>
		<li>il prodotto per il quale viene usato segue
		procedure così come sono documentate sul
		sito www.debian.org (per esempio la creazione di
		CD ufficiali), o </li>
		<li>una approvazione ufficiale è data da Debian per
		il suo uso per questo scopo. </li>
	</ul>
	</li>
	<li>Può essere usato se una parte ufficiale di Debian
	(che segua la regola in 1) compone un prodotto completo, ed ovviamente
	solo questa parte è ufficialmente approvata </li>
	<li>Ci riserviamo il diritto di revoca della licenza per un
	prodotto</li>
</ol>

<p>È già stato dato il permesso di usare il logo riservato su abbigliamento
(magliette, cappelli, ecc.) se vengono fatti da sviluppatori ufficiali Debian
e venduti non a scopo di lucro.</p>
</td>
<td>
<officiallogotable>
</td>
</tr>
</table>

<hr />

<h2>A proposito dei loghi Debian</h2>

<p>
I loghi Debian sono stati scelti dal voto degli sviluppatori Debian nel
1999 e sono stati creati da <a href="mailto:rsilva@debian.org">Raul
Silva</a>. Il colore rosso utilizzato nel carattere è denominato
<strong>rosso rubino 2X CVC</strong>; attualmente sono equivalenti il
PANTONE rosso forte C (rappresentato in RGB come #CE0056) o il PANTONE
rosso rubino C (rappresentato in RGB come #CE0058). È possibile trovare
ulteriori dettagli sui loghi, sul motivo per cui il PANTONE Rosso Rubino
2X CVC è stato sostituito e sui rossi equivalenti <a
href="https://wiki.debian.org/DebianLogo">sulla pagina Debian Logo del
Wiki</a>.
</p>

<h2>Altre immagini promozionali</h2>

<h3>Bottoni Debian</h3>

<p><img class="ico" src="button-1.gif" alt="[Debian GNU/Linux]">
Questo è il primo bottone fatto per il Progetto.
La licenza per questo logo è equivalente alla licenza di libero utilizzo.
Questo logo è stato creato da <a href="mailto:csmall@debian.org">Craig
Small</a>.

<p>Questi sono vari bottoni che sono stati fatti per Debian:
<br />
<morebuttons>

<h3>Logo di Debian Diversity </h3>

<p>
Esiste una variante del logo Debian per la promozione della diversità
nella nostra comunità, è chiamato logo di Debian Diversity:
<br/>
<img src="diversity-2019.png" alt="[Debian Diversity logo]" />
<br/>
Il logo è stato creato da <a
href="https://wiki.debian.org/ValessioBrito">Valessio Brito</a> ed è
licenziato con GPLv3. Il sorgente (in formato SVG) è disponibile nel
<a href="https://gitlab.com/valessiobrito/artwork">repository Git</a>
dell'autore.
</p>

<h3>Adesivo esagonale Debian</h3>

<p>
Questo è un adesivo conforme alle <a
href="https://github.com/terinjokes/StickerConstructorSpec">specifiche per
adesivi esagonali</a>:
<br/>
<img src="hexagonal.png" alt="[Debian GNU/Linux]" />
<br/>
Il sorgente (in formato SVG) e un Makefile per generare le anteprime in
png e svg previews sono disponibili nel <a
href="https://salsa.debian.org/debian/debian-flyers/tree/master/hexagonal-sticker">repository
Debian flyers</a>.

La licenza dell'adesivo è equivalente alla licenza di libero utilizzo.
L'adesivo è stato creato da <a href="mailto:valhalla@trueelena.org">Elena
Grandi</a>.
</p>

<h3>Altre immagini</h3>

<p>Si possono ottenere altre immagini carine da siti come:</p>

<table summary="Altri siti di web art Debian"><tr>
<td valign="bottom" align="center"><a
href="http://gnuart.onshore.com/"><img src="gnuart.png" alt="GNU/art" /></a>
<br />
<a href="http://gnuart.onshore.com/">GNU/art</a>
</td>
</tr></table>

<p><a href="tshirt-lt99.jpg">Tshirt art</a> dal LinuxTag-99.</p>

<p>Se vuoi aiutare a pubblicizzare Debian nel web puoi utilizzare uno
dei nostri <a href="../banners/">banner</a>.</p>

#
# Logo font: Poppl Laudatio Condensed
#
# https://lists.debian.org/debian-www/2003/08/msg00261.html
#
